#ifndef TCA9539_H
#define TCA9539_H
#include "bsp_i2c.h"
#define CMDINPUT0	0x00
#define CMDINPUT1	0x01
#define CMDOUTPUT0	0x02
#define CMDOUTPUT1	0x03
#define CMDINVER0	0x04
#define CMDINVER1	0x05
#define CMDCFG0		0x06
#define CMDCFG1		0x07


#define EXPort0     0x00
#define EXPort1     0x01
#define EXPin0      0x01
#define EXPin1      0x02
#define EXPin2      0x04
#define EXPin3      0x08
#define EXPin4      0x10
#define EXPin5      0x20
#define EXPin6      0x40
#define EXPin7      0x80
#define ValueH      true
#define ValueL      false
#ifdef __cplusplus
extern "C" {
#endif
void TCA9539_init(void);
void ledcontrol(uint8_t ledstatus);
void TCA9539_CFG(uint8_t cfg0,uint8_t cfg1);
uint8_t TCA9539_ReadPort(uint8_t port);
void TCA9539_SetPORT(uint8_t port, uint8_t Pin_data, bool value);
#ifdef __cplusplus
};
#endif

#endif