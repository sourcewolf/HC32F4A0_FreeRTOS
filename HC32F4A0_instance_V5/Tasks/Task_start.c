#include "Task_start.h"
#include "bsp_timer6_freq_meter.h"
#include "bsp_mco.h"
#include "bsp_i2c.h"
#include "AT24C02.h"
#include "TCA9539.h"
TaskHandle_t Hd_Task_Start;
void Task_START(void *param)
{
    while(1)
    {
		ledcontrol(0xFF);//
		vTaskDelay(500/portTICK_PERIOD_MS);
		ledcontrol(0x00);
		vTaskDelay(500/portTICK_PERIOD_MS);
    }
}
void OS_Start(void)
{
	xTaskCreate(Task_START,(const char *)"LED", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+3, &Hd_Task_Start );		    
    vTaskStartScheduler();
}
