#ifndef TASK_START_H
#define TASK_START_H
#include "cmsis_os.h"
#include "hc32_ddl.h"

void OS_Start(void);

#endif
