#ifndef BSP_IRQN_H
#define BSP_IRQN_H

#define TIMER63_CAP_IRQn	Int000_IRQn
#define TIMER63_OVF_IRQn	Int001_IRQn
#define TIMER62_OVF_IRQn	Int002_IRQn
#define TIMER01_CHA_IRQn	Int003_IRQn
#define TIMERA5_OVF_IRQn	Int004_IRQn
#define TIMERA4_CMP_IRQn	Int005_IRQn
#define TIMERA4_OVF_IRQn	Int006_IRQn
#define ADC_COA_IRQn        Int007_IRQn
#define MAU_SQRT_IRQn       Int008_IRQn
#define USART1_RTO_IRQn     Int009_IRQn
#define USART1_RXDMA_IRQn   Int010_IRQn
#define USART1_ER_IRQn      Int011_IRQn
#define USART4_RXDMA_IRQn   Int012_IRQn
#define USART4_RTO_IRQn     Int013_IRQn
#define USART4_ER_IRQn      Int014_IRQn
#define SPI4_ER_IRQn        Int015_IRQn
#define SPI4_RX_IRQn        Int016_IRQn
#define USART1_RX_IRQn      Int017_IRQn
#define DMA1_TC6_IRQn       Int018_IRQn
#define SPI4_IRQn           Int019_IRQn


#define KEYSCAN_ROW0_IRQn       (Int035_IRQn)
#define KEYSCAN_ROW1_IRQn       (Int036_IRQn)
#define KEYSCAN_ROW2_IRQn       (Int037_IRQn)


#define APP_CAN_IRQn        Int092_IRQn

#endif

