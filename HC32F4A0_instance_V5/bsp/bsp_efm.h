#ifndef BSP_EFM_H
#define BSP_EFM_H
#include "hc32_ddl.h"
typedef union
{
    uint8_t data8[8192];
    uint16_t data16[4096];
    uint32_t data32[2048];
}EFM_data_t;
typedef enum
{
    STARTFROM_BANK0 = Disable,
    STARTFROM_BANK1 = Enable
}originbankslect_t;
#ifdef __cplusplus
extern "C" {
#endif
void bsp_efm_write(uint32_t *writebuf, uint32_t addr, uint32_t len);
void bsp_testefm(void);
void bsp_swapflash(originbankslect_t selectedbank);
void bsp_test_copybank02bank1(void);
originbankslect_t bsp_getstartbank(void);
void bsp_showCurrntEFMbank(void);
void calljumpapp(void);
void cacheoff(void);
void cacheon(void);
#ifdef __cplusplus
};
#endif

#endif

