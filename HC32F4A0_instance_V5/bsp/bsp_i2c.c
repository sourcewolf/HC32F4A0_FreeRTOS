#include "bsp_i2c.h"
M4_I2C_TypeDef* pstc_int_I2Cx = NULL;
uint8_t * p_int_data = NULL;
void HW_I2C_Port_Init(void)
{
    stc_gpio_init_t stcGpioInit;
	GPIO_StructInit(&stcGpioInit);
	stcGpioInit.u16PinDir = PIN_DIR_OUT;
    stcGpioInit.u16PinDrv = PIN_DRV_LOW;
    stcGpioInit.u16PinIType = PIN_ITYPE_CMOS;
	GPIO_Unlock();
	GPIO_Init(I2Cx_SCL_PORT, I2Cx_SCL_Pin, &stcGpioInit);
    GPIO_Init(I2Cx_SDA_PORT, I2Cx_SDA_Pin, &stcGpioInit);
	GPIO_Lock();
    int i =1000;
    while(i--)
    {
        GPIO_TogglePins(I2Cx_SCL_PORT, I2Cx_SCL_Pin);
        GPIO_TogglePins(I2Cx_SDA_PORT, I2Cx_SDA_Pin);
        DDL_DelayUS(100);
    }
	GPIO_Unlock();
	GPIO_SetFunc(I2Cx_SCL_PORT, I2Cx_SCL_Pin, I2Cx_SCL_FUNC, Disable);
    GPIO_SetFunc(I2Cx_SDA_PORT, I2Cx_SDA_Pin, I2Cx_SDA_FUNC, Disable);
	GPIO_Lock();
}
/* funcion name: bsp_i2c_init
*功能：I2C 初始化
*输入参数：pstcI2Cx :I2Cx_UNIT，x = 1,2,3,4,5,6，baudrate: Hz,最大400000
*输出参数：无
*/
uint8_t bsp_i2c_init(M4_I2C_TypeDef* pstcI2Cx,uint32_t baudrate)
{
    float32_t fErr;

	stc_clk_freq_t freq_clk;
	stc_i2c_init_t stcI2cInit;	
	CLK_GetClockFreq(&freq_clk);	
	PWC_Unlock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);
	if(pstcI2Cx == I2C1_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C1_CLK,Enable);
	}
	else if(pstcI2Cx == I2C2_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C2_CLK,Enable);
	}
	else if(pstcI2Cx == I2C3_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C3_CLK,Enable);
	}
	else if(pstcI2Cx == I2C4_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C4_CLK,Enable);
	}
	else if(pstcI2Cx == I2C5_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C5_CLK,Enable);
	}
	else if(pstcI2Cx == I2C6_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C6_CLK,Enable); 
	}
	else
	{
		return 1;
	}
	PWC_Lock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);
	HW_I2C_Port_Init();		
    I2C_DeInit(pstcI2Cx);
    
    I2C_StructInit(&stcI2cInit);
    stcI2cInit.u32Baudrate = baudrate;
	stcI2cInit.u32SclTime = 0U;
    stcI2cInit.u32ClkDiv = I2C_CLK_DIV16;
    I2C_Init(pstcI2Cx, &stcI2cInit,&fErr);
    
    I2C_Cmd(pstcI2Cx, Enable);
	I2C_FastAckCmd(pstcI2Cx, Disable);
	return I2C_RET_OK;
}
/* funcion name: bsp_i2c_deinit
*功能：I2C 反初始化
*输入参数：pstcI2Cx :I2Cx_UNIT，x = 1,2,3,4,5,6，
*输出参数：无
*/
uint8_t bsp_i2c_deinit(M4_I2C_TypeDef* pstcI2Cx)
{   
	PWC_Unlock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);
    I2C_DeInit(pstcI2Cx);
	if(pstcI2Cx == I2C1_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C1_CLK,Disable);
	}
	else if(pstcI2Cx == I2C2_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C2_CLK,Disable);
	}
	else if(pstcI2Cx == I2C3_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C3_CLK,Disable);
	}
	else if(pstcI2Cx == I2C4_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C4_CLK,Disable);
	}
	else if(pstcI2Cx == I2C5_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C5_CLK,Disable);
	}
	else if(pstcI2Cx == I2C6_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C6_CLK,Disable); 
	}
	else
	{
		return 1;
	}
       
	PWC_Lock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);	   
	return I2C_RET_OK;
}
static inline uint8_t bsp_i2c_start(M4_I2C_TypeDef* pstcI2Cx)
{
    uint32_t u32TimeOut;
    I2C_GenerateStart(pstcI2Cx);
	u32TimeOut = TIMEOUT;
	while(Reset == I2C_GetStatus(pstcI2Cx, I2C_SR_TEMPTYF))//数据寄存器为空
	{
		if(0==u32TimeOut--)
		{
			if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
			{
				I2C_GenerateStop(pstcI2Cx);//停止释放总线
			}			
			return I2C_TIMEROUT;
		}
	}
    return I2C_RET_OK;
}
static inline uint8_t bsp_i2c_sendonebyte(M4_I2C_TypeDef* pstcI2Cx, uint8_t data)
{
    uint32_t u32TimeOut;
    I2C_WriteDataReg(pstcI2Cx, data);
    u32TimeOut = TIMEOUT;
    while(Reset == I2C_GetStatus(pstcI2Cx, I2C_SR_TENDF))//数据寄存器为空
    {
        if(0==(u32TimeOut--))
        {
            if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
            {
                I2C_GenerateStop(pstcI2Cx);//停止释放总线
            }			
            return I2C_TIMEROUT;
        }
    }	
    u32TimeOut = TIMEOUT;
    while(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_NACKF))//等待应答
    {
        if(0 == (u32TimeOut--)) 
        {
            if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
            {
                I2C_GenerateStop(pstcI2Cx);//停止释放总线
            }			
            return I2C_TIMEROUT;
        }
    }
    return I2C_RET_OK;	
}
static inline uint8_t bsp_i2c_senddevaddr(M4_I2C_TypeDef* pstcI2Cx, uint8_t devaddr)
{
    uint32_t u32TimeOut;
    I2C_WriteDataReg(pstcI2Cx, devaddr);
    if((devaddr & 0x01) == 0)//读指令不检查TENDF
    {
        u32TimeOut = TIMEOUT;
        while(Reset == I2C_GetStatus(pstcI2Cx, I2C_SR_TENDF))//数据寄存器为空
        {
            if(0==(u32TimeOut--))
            {
                if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
                {
                    I2C_GenerateStop(pstcI2Cx);//停止释放总线
                }			
                return I2C_TIMEROUT;
            }
        }
    }    
    else{
        u32TimeOut = TIMEOUT;
        while(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_TRA))//等待转换为读状态，修复读指令从机回NAK，没有stop的问题。
        {
            if(0==(u32TimeOut--))
            {
                if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
                {
                    I2C_GenerateStop(pstcI2Cx);//停止释放总线
                }			
                return I2C_TIMEROUT;
            }
        }
    }
    u32TimeOut = TIMEOUT;
    while(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_NACKF))//等待应答
    {
        if(0 == (u32TimeOut--)) 
        {
            if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
            {
                I2C_GenerateStop(pstcI2Cx);//停止释放总线
            }			
            return I2C_BADADDR;
        }
    }
    return I2C_RET_OK;	
}
/*
ack = 0 回ACK， ack != 0 回NAK
*/
static inline uint8_t bsp_i2c_readonebyte(M4_I2C_TypeDef* pstcI2Cx, uint8_t *data, uint8_t ack)
{
    uint32_t u32TimeOut;
    u32TimeOut = TIMEOUT;
    while(Reset == I2C_GetStatus(pstcI2Cx, I2C_SR_RFULLF))
    {
        if(0 == (u32TimeOut--))
        {
            I2C_GenerateStop(pstcI2Cx);//停止释放总线
            return I2C_TIMEROUT;
        }
    }
#ifndef AUTOACK			
    if(ack)
    {
        I2C_AckConfig(pstcI2Cx, I2C_NACK);
    }
    else
    {
        I2C_AckConfig(pstcI2Cx, I2C_ACK);//ACK
    }
#endif	        
    *data = I2C_ReadDataReg(pstcI2Cx);//I2C_ReadData(I2C1_UNIT);
#ifdef AUTOACK			
    if(ack)
    {
        I2C_AckConfig(pstcI2Cx, I2c_NACK);//NAK
    }
    else
    {
        I2C_AckConfig(pstcI2Cx, I2c_ACK);//ACK
    }
#endif
    return I2C_RET_OK;
}
static inline uint8_t bsp_i2c_senddata(M4_I2C_TypeDef* pstcI2Cx,const uint8_t *data, uint16_t len)
{
	uint16_t pos;
    uint8_t status;
    for(pos = 0;pos<len;pos++)
	{
		status = bsp_i2c_sendonebyte(pstcI2Cx,data[pos]);
        if(status != I2C_RET_OK)
        {
            return status;
        }
	}
    return I2C_RET_OK;
}
static inline uint8_t bsp_i2c_Recdata(M4_I2C_TypeDef* pstcI2Cx,uint8_t *data, uint16_t len)
{
    uint8_t status;
	uint16_t pos;
    for(pos = 0;pos<len;pos++)
	{
        status = bsp_i2c_readonebyte(pstcI2Cx,data,(pos == (len-1)));
        if(status != I2C_RET_OK)
        {
            return status;
        }        
	}
    return I2C_RET_OK;
}
static inline uint8_t bsp_i2c_restart(M4_I2C_TypeDef* pstcI2Cx)
{
    uint32_t u32TimeOut;
    I2C_ClearStatus(pstcI2Cx, I2C_CLR_STARTFCLR);
	I2C_GenerateReStart(pstcI2Cx);//发送Restart;
	u32TimeOut = TIMEOUT;
	while((Reset == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY)) ||
            (Reset == I2C_GetStatus(pstcI2Cx, I2C_SR_STARTF)))
    {
        if(0 == (u32TimeOut--)) 
		{
			I2C_GenerateStop(pstcI2Cx);//停止释放总线
			return I2C_RET_ERROR;
		}
    }
	u32TimeOut = TIMEOUT;
	while(Reset == I2C_GetStatus(pstcI2Cx, I2C_SR_TEMPTYF))//数据寄存器为空
	{
		if(0==u32TimeOut--)
		{
			if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
			{
				I2C_GenerateStop(pstcI2Cx);//停止释放总线
			}			
			return I2C_TIMEROUT;
		}
	}
    return I2C_RET_OK;
}
static inline uint8_t bsp_i2c_stop(M4_I2C_TypeDef* pstcI2Cx)
{
    uint32_t u32TimeOut;
    u32TimeOut = TIMEOUT;
	do{
		I2C_GenerateStop(pstcI2Cx);//产生停止位
		if(0 == (u32TimeOut--)) 
			{
				return I2C_TIMEROUT;
			}
	}while(Reset == I2C_GetStatus(pstcI2Cx, I2C_SR_STOPF));//等待停止
    return I2C_RET_OK;
}
inline uint8_t I2C_Write_data(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr,uint8_t *addr, uint8_t addrsize, const uint8_t *data, uint16_t len)
{
	uint8_t status;	
    if(len == 0)
    {
        return I2C_BADPARA;
    }
    if(addrsize == 0)
    {
        return I2C_BADPARA;
    }
	if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))//多任务通信时，抢占总线判断
	{
        
		return I2C_BUSY;
	}
	if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_NACKF))//启动I2C前先清除上一次读写失败的NAK标志，否则无法写数据，多任务抢占时，不能加这一段
    {
        I2C_ClearStatus(pstcI2Cx,I2C_CLR_NACKFCLR);
    }
	status = bsp_i2c_start(pstcI2Cx);
    if(status != I2C_RET_OK)
    {
        return status;
    }
	status = bsp_i2c_senddevaddr(pstcI2Cx,DeviceAddr<<1);
    if(status != I2C_RET_OK)
    {
        return status;
    }
    status = bsp_i2c_senddata(pstcI2Cx,addr,addrsize);
    if(status != I2C_RET_OK)
    {
        return status;
    }
	status = bsp_i2c_senddata(pstcI2Cx,data,len);
    if(status != I2C_RET_OK)
    {
        return status;
    }
    status = bsp_i2c_stop(pstcI2Cx);
    if(status != I2C_RET_OK)
    {
        return status;
    }
	return I2C_RET_OK;
}

inline uint8_t I2C_Read_data(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr,uint8_t *addr, uint8_t addrsize, uint8_t *data, uint16_t len)
{
	uint8_t status;	
    if(len == 0)
    {
        return I2C_BADPARA;
    }
    if(addrsize == 0)
    {
        return I2C_BADPARA;
    }
    if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
	{
		return I2C_BUSY;
	}	
    if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_NACKF))//启动I2C前先清除上一次读写失败的NAK标志，否则无法写数据
	{
		I2C_ClearStatus(pstcI2Cx,I2C_CLR_NACKFCLR);
	}
    status = bsp_i2c_start(pstcI2Cx);
#ifndef AUTOACK	
	I2C_FastAckCmd(pstcI2Cx,Disable);//非自动写ACK
#endif	
    if(status != I2C_RET_OK)
    {
        return status;
    }
	status = bsp_i2c_senddevaddr(pstcI2Cx,DeviceAddr<<1);
    if(status != I2C_RET_OK)
    {
        return status;
    }
    status = bsp_i2c_senddata(pstcI2Cx,addr,addrsize);
    if(status != I2C_RET_OK)
    {
        return status;
    }
    status = bsp_i2c_restart(pstcI2Cx);
    if(status != I2C_RET_OK)
    {
        return status;
    }
	status = bsp_i2c_senddevaddr(pstcI2Cx,(DeviceAddr<<1)|0x01);
    if(status != I2C_RET_OK)
    {
        return status;
    }
    status = bsp_i2c_Recdata(pstcI2Cx,data,len);
    if(status != I2C_RET_OK)
    {
        return status;
    }
    status = bsp_i2c_stop(pstcI2Cx);
    if(status != I2C_RET_OK)
    {
        return status;
    }
	return I2C_RET_OK;
}
inline uint8_t I2C_Write_Buffer(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr,const uint8_t *data, uint16_t len)
{
	uint8_t status;
	if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
	{
		if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_ARLOF))//仲裁失败
		{
//			I2C_ClearStatus(I2C_CH, I2C_CLR_ARLOFCLR);
			pstcI2Cx->CLR = 0xFFFF;
			I2C_GenerateStop(pstcI2Cx);//停止释放总线
		}
		return I2C_BUSY;
	}
    if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_NACKF))//启动I2C前先清除上一次读写失败的NAK标志，否则无法写数据
	{
		I2C_ClearStatus(pstcI2Cx,I2C_CLR_NACKFCLR);
	}
	 status = bsp_i2c_start(pstcI2Cx);
    if(status != I2C_RET_OK)
    {
        return status;
    }
	status = bsp_i2c_senddevaddr(pstcI2Cx,DeviceAddr<<1);
    if(status != I2C_RET_OK)
    {
        return status;
    }
	status = bsp_i2c_senddata(pstcI2Cx,data,len);
    if(status != I2C_RET_OK)
    {
        return status;
    }
    status = bsp_i2c_stop(pstcI2Cx);
    if(status != I2C_RET_OK)
    {
        return status;
    }
	return I2C_RET_OK;
}
inline uint8_t I2C_Read_Buffer(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr, uint8_t *data, uint16_t len)
{
	uint8_t status;	
    if(len == 0)
    {
        return I2C_BADPARA;
    }
    if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
    {
		return I2C_BUSY;
	}	
    if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_NACKF))//启动I2C前先清除上一次读写失败的NAK标志，否则无法写数据
	{
		I2C_ClearStatus(pstcI2Cx,I2C_CLR_NACKFCLR);
	}
	 status = bsp_i2c_start(pstcI2Cx);
#ifndef AUTOACK	
	I2C_FastAckCmd(pstcI2Cx,Disable);//非自动写ACK
#endif	    
    if(status != I2C_RET_OK)
    {
        return status;
    }
	status = bsp_i2c_senddevaddr(pstcI2Cx,(DeviceAddr<<1)|0x01);
    if(status != I2C_RET_OK)
    {
        return status;
    }
    status = bsp_i2c_Recdata(pstcI2Cx,data,len);
    if(status != I2C_RET_OK)
    {
        return status;
    }
    status = bsp_i2c_stop(pstcI2Cx);
    if(status != I2C_RET_OK)
    {
        return status;
    }
	return I2C_RET_OK;
}

void DMA_TX_TC_Callback(void)
{
    uint8_t status;
//    uint32_t u32TimeOut;
    I2C_FastAckCmd(pstc_int_I2Cx,Disable);//非自动写ACK
    status = bsp_i2c_readonebyte(pstc_int_I2Cx,p_int_data,1);
    if(status != I2C_RET_OK)
    {
        return;
    }  
    status = bsp_i2c_stop(pstc_int_I2Cx);
    if(status != I2C_RET_OK)
    {
        return ;
    }
}
void DMA_RX_TC_Callback(void)
{
    uint32_t u32TimeOut;
    pstc_int_I2Cx->CR3 |= 0x80;//非自动写ACK
    u32TimeOut = TIMEOUT;
		while(Reset == I2C_GetStatus(pstc_int_I2Cx, I2C_SR_RFULLF))
		{
			if(0 == (u32TimeOut--))
			{
				I2C_GenerateStop(pstc_int_I2Cx);//停止释放总线
				return;
			}
		}

		I2C_AckConfig(pstc_int_I2Cx, I2C_NACK);//NAK
    *p_int_data = pstc_int_I2Cx->DRR;   
    u32TimeOut = TIMEOUT;
	do{
		I2C_GenerateStop(pstc_int_I2Cx);//产生停止位
		if(0 == (u32TimeOut--)) 
			{
				return;
			}
	}while(Reset == I2C_GetStatus(pstc_int_I2Cx, I2C_SR_STOPF));//等待停止;
}
/* funcion name: bsp_i2c_dma_init
*功能：I2C
*输入参数：mco_num :暂时无效，请输入0，freq:暂时无效，请输入0
*输出参数：无
*/
uint8_t bsp_i2c_dma_init(M4_I2C_TypeDef* pstcI2Cx,uint32_t baudrate)
{
    float32_t fErr;

	stc_clk_freq_t freq_clk;
	stc_i2c_init_t stcI2cInit;	
	CLK_GetClockFreq(&freq_clk);	
	PWC_Unlock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);
	if(pstcI2Cx == I2C1_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C1_CLK,Enable);
	}
	else if(pstcI2Cx == I2C2_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C2_CLK,Enable);
	}
	else if(pstcI2Cx == I2C3_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C3_CLK,Enable);
	}
	else if(pstcI2Cx == I2C4_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C4_CLK,Enable);
	}
	else if(pstcI2Cx == I2C5_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C5_CLK,Enable);
	}
	else if(pstcI2Cx == I2C6_UNIT)
	{
		PWC_Fcg1PeriphClockCmd(I2C6_CLK,Enable); 
	}
	else
	{
		return 1;
	}
	PWC_Lock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);
			
    I2C_DeInit(pstcI2Cx);
    
    I2C_StructInit(&stcI2cInit);
    stcI2cInit.u32Baudrate = baudrate;
	stcI2cInit.u32SclTime = 5U;
    stcI2cInit.u32ClkDiv = I2C_CLK_DIV16;
    I2C_Init(pstcI2Cx, &stcI2cInit,&fErr);
    
    I2C_Cmd(pstcI2Cx, Enable);
	I2C_FastAckCmd(M4_I2C1, Disable);
    bsp_dma_init(I2C_DMA_UNIT, DMA_RX_CH, DMA_Mode_RXsrc, DMA_Mode_RXdes, DataWidth);
    bsp_dma_init(I2C_DMA_UNIT, DMA_TX_CH, DMA_Mode_TXsrc, DMA_Mode_TXdes, DataWidth);
	return I2C_RET_OK;
}
inline uint8_t I2C_DMA_Write_data(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr,uint8_t *addr, uint8_t addrsize, const uint8_t *data, uint16_t len)
{
	uint32_t u32TimeOut;
//	uint8_t pos; 
    uint8_t status;
//    printf("I2C_DMA_Write_data\r\n");
	if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
	{
//        printf("I2C_DMA_Write_data Busy\r\n");
		return I2C_BUSY;
	}
    if(len == 0)
    {
        return I2C_BADPARA;
    }
    if(addrsize == 0)
    {
        return I2C_BADPARA;
    }
    if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_NACKF))//启动I2C前先清除上一次读写失败的NAK标志，否则无法写数据
	{
		I2C_ClearStatus(pstcI2Cx,I2C_CLR_NACKFCLR);
	}
    status = bsp_i2c_start(pstcI2Cx);
    if(status != I2C_RET_OK)
    {
        return status;
    }
	status = bsp_i2c_senddevaddr(pstcI2Cx,DeviceAddr<<1);
    if(status != I2C_RET_OK)
    {
        return status;
    }
    status = bsp_i2c_senddata(pstcI2Cx,addr,addrsize-1);
    if(status != I2C_RET_OK)
    {
        return status;
    }
    bsp_interrupt_callback_regist(DMA_TX_INT,Int001_IRQn,(void *)DMA_TX_TC_Callback);
    bsp_set_interrupt_priority(Int001_IRQn,DDL_IRQ_PRIORITY_14);
    pstc_int_I2Cx = pstcI2Cx;
    bsp_dma_SetDesAddr(I2C_DMA_UNIT, DMA_TX_CH, (uint32_t)&(pstcI2Cx->DTR));
    bsp_dma_SetSrcAddr(I2C_DMA_UNIT, DMA_TX_CH, (uint32_t)data);
    bsp_dma_set_count(I2C_DMA_UNIT,DMA_TX_CH,len);//
    bsp_dma_ch_enable(I2C_DMA_UNIT,DMA_TX_CH,Enable);
    if(pstcI2Cx == M4_I2C1)
    {
        bsp_dma_set_TrigSrc(I2C_DMA_UNIT,DMA_TX_CH,EVT_I2C1_TXI);//
    }
    if(pstcI2Cx == M4_I2C2)
    {
        bsp_dma_set_TrigSrc(I2C_DMA_UNIT,DMA_TX_CH,EVT_I2C2_TXI);//
    }
    if(pstcI2Cx == M4_I2C3)
    {
        bsp_dma_set_TrigSrc(I2C_DMA_UNIT,DMA_TX_CH,EVT_I2C3_TXI);//
    }    
	I2C_WriteDataReg(pstcI2Cx, addr[addrsize - 1]);//发最后一个地址字节
	u32TimeOut = TIMEOUT;
	while(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_NACKF))//等待应答
	{
		if(0 == (u32TimeOut--)) 
		{
			if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
			{
                 bsp_dma_ch_enable(I2C_DMA_UNIT,DMA_TX_CH,Disable);
				I2C_GenerateStop(pstcI2Cx);//停止释放总线
			}			
			return I2C_TIMEROUT;
		}
	}
	return I2C_RET_OK;
}
inline uint8_t I2C_DMA_Read_data(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr,uint8_t* addr,uint8_t addrsize, uint8_t *data, uint16_t len)
{
    uint8_t status;
//	uint32_t u32TimeOut;
//	uint8_t pos;
    if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
	{
		return I2C_BUSY;
	}
    if(len == 0)
    {
        return I2C_BADPARA;
    }
    if(addrsize == 0)
    {
        return I2C_BADPARA;
    }
	if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_NACKF))//启动I2C前先清除上一次读写失败的NAK标志，否则无法写数据
	{
		I2C_ClearStatus(pstcI2Cx,I2C_CLR_NACKFCLR);
	}	
	status = bsp_i2c_start(pstcI2Cx);
    if(status != I2C_RET_OK)
    {
        return status;
    }
//    pstcI2Cx->CR3_f.FACKEN = 0;//自动写ACK
    I2C_FastAckCmd(pstcI2Cx,Enable);//自动写ACK
	status = bsp_i2c_senddevaddr(pstcI2Cx,DeviceAddr<<1);
    if(status != I2C_RET_OK)
    {
        return status;
    }
    status = bsp_i2c_senddata(pstcI2Cx,addr,addrsize);
    if(status != I2C_RET_OK)
    {
        return status;
    }
	status = bsp_i2c_restart(pstcI2Cx);
    if(status != I2C_RET_OK)
    {
        return status;
    }
    if(len>1)//读长大于1时启用DMA
    {
        bsp_interrupt_callback_regist(DMA_RX_INT,Int002_IRQn,(void *)DMA_RX_TC_Callback);
        bsp_set_interrupt_priority(Int002_IRQn,DDL_IRQ_PRIORITY_14);
        pstc_int_I2Cx = pstcI2Cx;
        p_int_data = (uint8_t*)data+len-1;
        bsp_dma_SetDesAddr(I2C_DMA_UNIT, DMA_RX_CH, (uint32_t)data);
        bsp_dma_SetSrcAddr(I2C_DMA_UNIT, DMA_RX_CH, (uint32_t)&(pstcI2Cx->DRR));
        bsp_dma_set_count(I2C_DMA_UNIT,DMA_RX_CH,len-1);
        bsp_dma_ch_enable(I2C_DMA_UNIT,DMA_RX_CH,Enable);
        if(pstcI2Cx == M4_I2C1)
        {
            bsp_dma_set_TrigSrc(I2C_DMA_UNIT,DMA_RX_CH,EVT_I2C1_RXI);//先实现I2C1,其他后续再补上
        } 
        if(pstcI2Cx == M4_I2C2)
        {
            bsp_dma_set_TrigSrc(I2C_DMA_UNIT,DMA_RX_CH,EVT_I2C2_RXI);//先实现I2C1,其他后续再补上
        }   
        if(pstcI2Cx == M4_I2C3)
        {
            bsp_dma_set_TrigSrc(I2C_DMA_UNIT,DMA_RX_CH,EVT_I2C3_RXI);//先实现I2C1,其他后续再补上
        }     
    }        
	status = bsp_i2c_senddevaddr(pstcI2Cx,(DeviceAddr<<1)|0x01);
    if(status != I2C_RET_OK)
    {
        bsp_dma_ch_enable(I2C_DMA_UNIT,DMA_RX_CH,Disable);
        return status;
    }
    if(len==1)//读长为1时，不启用DMA直接读取，回NAK
    {
        I2C_FastAckCmd(pstcI2Cx,Disable);//非自动写ACK
        status = bsp_i2c_Recdata(pstcI2Cx,data,1);
        if(status != I2C_RET_OK)
        {
            return status;
        }
        status = bsp_i2c_stop(pstcI2Cx);
        if(status != I2C_RET_OK)
        {
            return status;
        }
    }
	return I2C_RET_OK;
}
inline uint8_t I2C_DMA_Write_Buffer(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr, const uint8_t *data, uint16_t len)
{
    uint8_t status;
	uint32_t u32TimeOut;
//	uint8_t pos;  
//    printf("I2C_DMA_Write_Buffer\r\n");
	if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
	{
//        printf("I2C_DMA_Write_Buffer Busy\r\n");
		return I2C_BUSY;
	}
    if(len == 0)
    {
        return I2C_BADPARA;
    }
    if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_NACKF))//启动I2C前先清除上一次读写失败的NAK标志，否则无法写数据
	{
		I2C_ClearStatus(pstcI2Cx,I2C_CLR_NACKFCLR);
	}
	status = bsp_i2c_start(pstcI2Cx);
    if(status != I2C_RET_OK)
    {
        return status;
    }
	status = bsp_i2c_senddevaddr(pstcI2Cx,DeviceAddr<<1);
    if(status != I2C_RET_OK)
    {
        return status;
    }
    if(len >1)
    {
        bsp_interrupt_callback_regist(DMA_TX_INT,Int001_IRQn,(void *)DMA_TX_TC_Callback);
        bsp_set_interrupt_priority(Int001_IRQn,DDL_IRQ_PRIORITY_14);
        pstc_int_I2Cx = pstcI2Cx;
        bsp_dma_SetDesAddr(I2C_DMA_UNIT, DMA_TX_CH, (uint32_t)&(pstcI2Cx->DTR));
        bsp_dma_SetSrcAddr(I2C_DMA_UNIT, DMA_TX_CH, (uint32_t)&data[1]);
        bsp_dma_set_count(I2C_DMA_UNIT,DMA_TX_CH,len-1);//
        bsp_dma_ch_enable(I2C_DMA_UNIT,DMA_TX_CH,Enable);
        if(pstcI2Cx == M4_I2C1)
        {
            bsp_dma_set_TrigSrc(I2C_DMA_UNIT,DMA_TX_CH,EVT_I2C1_TXI);//先实现I2C1,其他后续再补上 
        }
        if(pstcI2Cx == M4_I2C2)
        {
            bsp_dma_set_TrigSrc(I2C_DMA_UNIT,DMA_TX_CH,EVT_I2C2_TXI);//先实现I2C1,其他后续再补上 
        }
        if(pstcI2Cx == M4_I2C3)
        {
            bsp_dma_set_TrigSrc(I2C_DMA_UNIT,DMA_TX_CH,EVT_I2C3_TXI);//先实现I2C1,其他后续再补上 
        } 
    }
	I2C_WriteDataReg(pstcI2Cx, data[0]);
	u32TimeOut = TIMEOUT;
	while(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_NACKF))//等待应答
	{
		if(0 == (u32TimeOut--)) 
		{
			if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
			{
                bsp_dma_ch_enable(I2C_DMA_UNIT,DMA_TX_CH,Disable);
				I2C_GenerateStop(pstcI2Cx);//停止释放总线
			}			
			return I2C_TIMEROUT;
		}
	}
    if(len == 1)
    {
        status = bsp_i2c_stop(pstcI2Cx);
        if(status != I2C_RET_OK)
        {
            return status;
        }
    }
	return I2C_RET_OK;
}
inline uint8_t I2C_DMA_Read_Buffer(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr, uint8_t *data, uint16_t len)
{
    uint8_t status;
//	uint32_t u32TimeOut;
//	uint8_t pos;
    if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_BUSY))
	{
		return I2C_BUSY;
	}
    if(len == 0)
    {
        return I2C_BADPARA;
    }	
	if(Set == I2C_GetStatus(pstcI2Cx, I2C_SR_NACKF))//启动I2C前先清除上一次读写失败的NAK标志，否则无法写数据
	{
		I2C_ClearStatus(pstcI2Cx,I2C_CLR_NACKFCLR);
	}	
	status = bsp_i2c_start(pstcI2Cx);
    if(status != I2C_RET_OK)
    {
        return status;
    }
//    pstcI2Cx->CR3_f.FACKEN = 0;//自动写ACK
    I2C_FastAckCmd(pstcI2Cx,Enable);//自动写ACK
    if(len>1)//读长大于1时启用DMA
    {
        bsp_interrupt_callback_regist(DMA_RX_INT,Int002_IRQn,(void *)DMA_RX_TC_Callback);
        bsp_set_interrupt_priority(Int002_IRQn,DDL_IRQ_PRIORITY_14);
        pstc_int_I2Cx = pstcI2Cx;
        p_int_data = (uint8_t*)data+len-1;
        bsp_dma_SetDesAddr(I2C_DMA_UNIT, DMA_RX_CH, (uint32_t)data);
        bsp_dma_SetSrcAddr(I2C_DMA_UNIT, DMA_RX_CH, (uint32_t)&(pstcI2Cx->DRR));
        bsp_dma_set_count(I2C_DMA_UNIT,DMA_RX_CH,len-1);
        bsp_dma_ch_enable(I2C_DMA_UNIT,DMA_RX_CH,Enable);
        if(pstcI2Cx == M4_I2C1)
        {
            bsp_dma_set_TrigSrc(I2C_DMA_UNIT,DMA_RX_CH,EVT_I2C1_RXI);//先实现I2C1,其他后续再补上
        } 
        if(pstcI2Cx == M4_I2C2)
        {
            bsp_dma_set_TrigSrc(I2C_DMA_UNIT,DMA_RX_CH,EVT_I2C2_RXI);//先实现I2C1,其他后续再补上
        }   
        if(pstcI2Cx == M4_I2C3)
        {
            bsp_dma_set_TrigSrc(I2C_DMA_UNIT,DMA_RX_CH,EVT_I2C3_RXI);//先实现I2C1,其他后续再补上
        }     
    }        
	status = bsp_i2c_senddevaddr(pstcI2Cx,(DeviceAddr<<1)|0x01);
    if(status != I2C_RET_OK)
    {
        bsp_dma_ch_enable(I2C_DMA_UNIT,DMA_RX_CH,Disable);
        return status;
    }
    if(len==1)//读长为1时，不启用DMA直接读取，回NAK
    {
        I2C_FastAckCmd(pstcI2Cx,Enable);//自动写ACK
        status = bsp_i2c_Recdata(pstcI2Cx,data,1);
        if(status != I2C_RET_OK)
        {
            return status;
        }
        status = bsp_i2c_stop(pstcI2Cx);
        if(status != I2C_RET_OK)
        {
            return status;
        }
    }
	return I2C_RET_OK;
}