#ifndef BSP_MAU_H
#define BSP_MAU_H
#include "hc32_ddl.h"
#include "bsp_trng.h"
#ifdef __cplusplus
extern "C" {
#endif
en_result_t Sqrt_Example(void);
uint32_t Sin_Example(void);
void bsp_mau_test(void);
void bsp_mau_init(void);


#ifdef __cplusplus
};
#endif
#endif

