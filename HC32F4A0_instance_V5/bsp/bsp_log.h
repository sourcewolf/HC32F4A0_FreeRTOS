#ifndef BSP_LOG_H
#define BSP_LOG_H
//#include "SEGGER_RTT.h"
#define LOG_EN
#define UARTPRINT
#if defined LOG_EN
#if defined (UARTPRINT)
#define LOG_INFO(...)           printf(__VA_ARGS__)//;\
                                printf("----File:.%s,Func: %s,line: %d\r\n", __FILE__,__func__,__LINE__)
#define LOG(...)                printf(__VA_ARGS__)//;\
                                printf("----File:.%s,Func: %s,line: %d\r\n", __FILE__,__func__,__LINE__)
#define LOGERR(format, ...)     printf("Error!");\
                                printf(format, __VA_ARGS__);\
                                printf("----File:.%s,Func: %s,line: %d\r\n", __FILE__,__func__,__LINE__)
#define LOGWARNING(format, ...) printf("Warning!");\
                                printf(format, __VA_ARGS__);\
                                printf("----File:.%s,Func: %s,line: %d\r\n", __FILE__,__func__,__LINE__)
#else
#if defined (__ARMCC_VERSION) && (__ARMCC_VERSION >= 6100100)
#define LOG_INFO(...)           SEGGER_RTT_WriteString(0, RTT_CTRL_TEXT_BRIGHT_WHITE);\
                                SEGGER_RTT_printf(0, __VA_ARGS__);\
                                SEGGER_RTT_printf(0, "----File: %s,Func: %s,line: %d\r\n", __FILE__,__func__,__LINE__)
#define LOG(...)                SEGGER_RTT_WriteString(0, RTT_CTRL_TEXT_BRIGHT_WHITE);\
                                SEGGER_RTT_printf(0, __VA_ARGS__);\
                                SEGGER_RTT_printf(0, "----File: %s,Func: %s,line: %d\r\n", __FILE__,__func__,__LINE__)
#define LOGERR(format, ...)     SEGGER_RTT_WriteString(0, RTT_CTRL_TEXT_BRIGHT_RED); \
                                SEGGER_RTT_printf(0,"Error!");\
                                SEGGER_RTT_printf(0, format, __VA_ARGS__);\
                                SEGGER_RTT_printf(0, "----File: %s,Func: %s,line: %d\r\n", __FILE__,__func__,__LINE__);\
                                SEGGER_RTT_WriteString(0, RTT_CTRL_TEXT_BRIGHT_WHITE)
#define LOGWARNING(format, ...)    SEGGER_RTT_WriteString(0, RTT_CTRL_TEXT_BRIGHT_YELLOW); \
                                SEGGER_RTT_printf(0,"Warning!");\
                                SEGGER_RTT_printf(0, format, __VA_ARGS__);\
                                SEGGER_RTT_printf(0, "----File: %s,Func: %s,line: %d\r\n", __FILE__,__func__,__LINE__);\
                                SEGGER_RTT_WriteString(0, RTT_CTRL_TEXT_BRIGHT_WHITE)
                            
#elif defined (__CC_ARM)
#define LOG_INFO(...)           SEGGER_RTT_printf(0, __VA_ARGS__);\
                                SEGGER_RTT_printf(0, "----File:.%s,Func:%s,line:%d\r\n", __FILE__,__func__,__LINE__)
#define LOG(format, ...)        SEGGER_RTT_printf(0, format, __VA_ARGS__);\
                                SEGGER_RTT_printf(0, "----File:.%s,Func:%s,line:%d\r\n", __FILE__,__func__,__LINE__)
#define LOGERR(format, ...)     SEGGER_RTT_WriteString(0, RTT_CTRL_TEXT_BRIGHT_RED); \
                                SEGGER_RTT_printf(0,"Error!");\
                                SEGGER_RTT_printf(0, format, __VA_ARGS__);\
                                SEGGER_RTT_printf(0, "----File:.%s,Func:%s,line:%d\r\n", __FILE__,__func__,__LINE__);\
                                SEGGER_RTT_WriteString(0, RTT_CTRL_TEXT_BRIGHT_WHITE)
#define LOGWARNING(format, ...)    SEGGER_RTT_WriteString(0, RTT_CTRL_TEXT_BRIGHT_YELLOW); \
                                SEGGER_RTT_printf(0,"Warning!");\
                                SEGGER_RTT_printf(0, format, __VA_ARGS__);\
                                SEGGER_RTT_printf(0, "----File:.%s,Func:%s,line:%d\r\n", __FILE__,__func__,__LINE__);\
                                SEGGER_RTT_WriteString(0, RTT_CTRL_TEXT_BRIGHT_WHITE)
#elif defined (__GNUC__) && !defined (__CC_ARM)
#define LOG_INFO(...) SEGGER_RTT_printf(0, __VA_ARGS__)
#define LOG(format, ...) SEGGER_RTT_printf(0, format, __VA_ARGS__)
#define LOGERR(format, ...)     SEGGER_RTT_WriteString(0, RTT_CTRL_TEXT_BRIGHT_RED); \
                                SEGGER_RTT_printf(0,"Error!");\
                                SEGGER_RTT_printf(0, format, __VA_ARGS__);\
                                SEGGER_RTT_printf(0, "----File:.%s,Func:%s,line:%d\r\n", __FILE__,__func__,__LINE__);\
                                SEGGER_RTT_WriteString(0, RTT_CTRL_TEXT_BRIGHT_WHITE)
#define LOGWARNING(format, ...)    SEGGER_RTT_WriteString(0, RTT_CTRL_TEXT_BRIGHT_YELLOW); \
                                SEGGER_RTT_printf(0,"Warning!");\
                                SEGGER_RTT_printf(0, format, __VA_ARGS__);\
                                SEGGER_RTT_printf(0, "----File:.%s,Func:%s,line:%d\r\n", __FILE__,__func__,__LINE__);\
                                SEGGER_RTT_WriteString(0, RTT_CTRL_TEXT_BRIGHT_WHITE)
#endif      //end of #if defined (__ARMCC_VERSION) && (__ARMCC_VERSION >= 6100100)


#endif      //end of #if defined (UARTPRINT)
#else
#define LOG_INFO(...)           
#define LOG(format, ...) 
#define LOGERR(format, ...)
#define LOGWARNING(format, ...) 
#endif      //end of #if defined LOG_EN 
#endif    //end of #ifndef BSP_LOG_H
