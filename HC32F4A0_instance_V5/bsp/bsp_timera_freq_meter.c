#include "bsp_timera_freq_meter.h"
uint32_t exfreq;
uint16_t Counter_ovf = 0;
void timerA_1ms_callback(void)
{
	;
}
void timerA_overflow_callback(void)
{
	Counter_ovf++;
}
void timerA_capture_callback(void)
{
	exfreq = (Counter_ovf*65000+M4_TMRA_4->CMPAR1)*10;
	Counter_ovf = 0;
}

void bsp_timerA_timer_1ms_init(void)//计时TIMER初始化
{
	uint32_t                       u32Period;
    stc_tmra_init_t         stcTIMaBaseCntCfg;
	stc_irq_signin_config_t        stcIrqRegiConf;
	TMRA_StructInit(&stcTIMaBaseCntCfg);
	stcTIMaBaseCntCfg.u32CntMode = TMRA_MODE_SAWTOOTH;
	stcTIMaBaseCntCfg.u32CntDir = TMRA_DIR_UP;
	stcTIMaBaseCntCfg.u32PCLKDiv = TMRA_PCLK_DIV8;
	stcTIMaBaseCntCfg.u32CntOvfOp = TMRA_OVF_CNT_CONTINUE;
	stcTIMaBaseCntCfg.u32PeriodVal = 15000;
//	PWC_Unlock(PWC_UNLOCK_CODE_1);
	PWC_Fcg2PeriphClockCmd(PWC_FCG2_TMRA_5, Enable);//开TIMERA5时钟
//	PWC_Lock(PWC_UNLOCK_CODE_1);
	TMRA_Init(M4_TMRA_5,&stcTIMaBaseCntCfg);
	TMRA_Start(M4_TMRA_5);
//	TMR6_Init(T_TMR_UNIT, &stcTIM6BaseCntCfg);
//	u32Period = 0x16E3600U;//100ms
//    TMR6_SetPeriodReg(T_TMR_UNIT, TMR6_PERIOD_REG_A, u32Period);	
//	TMR6_CountCmd(T_TMR_UNIT, Enable);	
//	
//	TMRA_IntCmd(M4_TMRA_5, TMRA_INT_OVF, Enable);
//	stcIrqRegiConf.enIRQn = TIMERA5_OVF_IRQn;                    /* Register INT_TMR61_GOVF Int to Vect.No.002 */
//    stcIrqRegiConf.enIntSrc = INT_TMRA_5_OVF;               /* Select Event interrupt of tmr61 */
//    stcIrqRegiConf.pfnCallback = &timerA_1ms_callback;   /* Callback function */
//    INTC_IrqSignIn(&stcIrqRegiConf);                        /* Registration IRQ */

//    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);            /* Clear Pending */
//    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);/* Set priority */
//    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);                  /* Enable NVIC */
}
void bsp_timerA_counter_init(void)
{
	uint32_t                       u32Period;
    stc_tmra_init_t         stcTIMaBaseCntCfg;
	stc_irq_signin_config_t        stcIrqRegiConf;
	TMRA_StructInit(&stcTIMaBaseCntCfg);
	stcTIMaBaseCntCfg.u32CntMode = TMRA_MODE_SAWTOOTH;
	stcTIMaBaseCntCfg.u32CntDir = TMRA_DIR_UP;
	stcTIMaBaseCntCfg.u32PCLKDiv = TMRA_PCLK_DIV8;
	stcTIMaBaseCntCfg.u32CntOvfOp = TMRA_OVF_CNT_CONTINUE;
	stcTIMaBaseCntCfg.u32PeriodVal = 65000;
	
	PWC_FCG0_Unlock();
	PWC_Unlock(PWC_UNLOCK_CODE_1);
	PWC_Fcg2PeriphClockCmd(PWC_FCG2_TMRA_4, Enable);//开TIMER6时钟
	PWC_Fcg0PeriphClockCmd(PWC_FCG0_AOS, Enable);
	PWC_Lock(PWC_UNLOCK_CODE_1);
	PWC_FCG0_Lock();
	
	TMRA_Init(M4_TMRA_4,&stcTIMaBaseCntCfg);
	TMRA_Start(M4_TMRA_4);
	GPIO_Unlock();
	GPIO_SetFunc(GPIO_PORT_F,GPIO_PIN_12,GPIO_FUNC_4_TIMA3_PWM2,PIN_SUBFUNC_DISABLE);//CLKB
	GPIO_Lock();
	TMRA_HwClkSrcCmd(M4_TMRA_4,TMRA_CLK_HW_UP_CLKAL_CLKBF,Enable);
	TMRA_SetTriggerSrc(M4_TMRA_4,TMRA_EVENT_USAGE_CAPT, EVT_TMR6_2_GOVF);
//	M4_AOS->TMRA_HTSSR0 = 
	M4_TMRA_4->CCONR1 = 0x0041;
	TMRA_IntCmd(M4_TMRA_4,TMRA_INT_CMP_CH1,Enable);
	TMRA_IntCmd(M4_TMRA_4,TMRA_INT_OVF,Enable);
	
	stcIrqRegiConf.enIRQn = TIMERA4_CMP_IRQn;                    /* Register INT_TMR61_GOVF Int to Vect.No.002 */
    stcIrqRegiConf.enIntSrc = INT_TMRA_4_CMP;               /* Select Event interrupt of tmr61 */
    stcIrqRegiConf.pfnCallback = &timerA_capture_callback;   /* Callback function */
    INTC_IrqSignIn(&stcIrqRegiConf);                        /* Registration IRQ */

    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);            /* Clear Pending */
    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);/* Set priority */
    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);  

	stcIrqRegiConf.enIRQn = TIMERA4_OVF_IRQn;                    /* Register INT_TMR61_GOVF Int to Vect.No.002 */
    stcIrqRegiConf.enIntSrc = INT_TMRA_4_OVF;               /* Select Event interrupt of tmr61 */
    stcIrqRegiConf.pfnCallback = &timerA_overflow_callback;   /* Callback function */
    INTC_IrqSignIn(&stcIrqRegiConf);                        /* Registration IRQ */

    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);            /* Clear Pending */
    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);/* Set priority */
    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);	
}




