#include "bsp_rmu.h"
#include "bsp_log.h"
uint32_t bsp_get_resetcause(void)
{
    uint32_t resetcause = M4_RMU->RSTF0;
    RMU_ClrStatus();
    return resetcause;
}
void bsp_show_resetcause(uint32_t resetflag)
{
    if(resetflag & RMU_RST_POWER_ON)
    {
        LOG_INFO("POR reset\r\n");
    }
    if(resetflag & RMU_RST_RESET_PIN)
    {
        LOG_INFO("Extern Pin Reset\r\n");
    }
    if(resetflag & RMU_RST_BROWN_OUT)
    {
        LOG_INFO("BOD Reset\r\n");
    }
    if(resetflag & RMU_RST_PVD1)
    {
        LOG_INFO("PVD1 Reset\r\n");
    }
    if(resetflag & RMU_RST_PVD2)
    {
        LOG_INFO("PVD2 Reset\r\n");
    }
    if(resetflag & RMU_RST_WDT)
    {
        LOG_INFO("WDT Reset\r\n");
    }
    if(resetflag & RMU_RST_SWDT)
    {
        LOG_INFO("SWDT Reset\r\n");
    }
    if(resetflag & RMU_RST_POWER_DOWN)
    {
        LOG_INFO("PowerDown Reset\r\n");
    }
    if(resetflag & RMU_RST_SOFTWARE)
    {
        LOG_INFO("Software Reset\r\n");
    }
    if(resetflag & RMU_RST_MPU_ERR)
    {
        LOG_INFO("MPU Error Reset\r\n");
    }
    if(resetflag & RMU_RST_RAM_PARITY_ERR)
    {
        LOG_INFO("RAM PARITY Error Reset\r\n");
    }
    if(resetflag & RMU_RST_RAM_ECC)
    {
        LOG_INFO("RAM ECC Error Reset\r\n");
    }
    if(resetflag & RMU_RST_CLK_ERR)
    {
        LOG_INFO("CLK Error Reset\r\n");
    }
    if(resetflag & RMU_RST_XTAL_ERR)
    {
        LOG_INFO("XTAL Error Reset\r\n");
    }
    if(resetflag & RMU_RST_LOCKUP)
    {
        LOG_INFO("CPU lockup Reset\r\n");
    }
    if(resetflag & RMU_RST_MULTI)
    {
        LOG_INFO("mulitple Reset\r\n");
    }
}





