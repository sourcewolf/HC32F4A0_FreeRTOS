#ifndef BSP_SPI_H
#define BSP_SPI_H
#include "hc32_ddl.h"
#include "bsp_interrupt.h"
#include "bsp_dma.h"

#define TIMEROUT                        10

#define SPI_RET_OK                      0
#define SPI_RET_ERROR                   1
#define SPI_BUSY						2
#define SPI_TIMEROUT					3
#define SPI_BADPARA                     4//参数错误

#define SPI_UNIT1           M4_SPI1
#define SPI_UNIT2           M4_SPI2
#define SPI_UNIT3           M4_SPI3
#define SPI_UNIT4           M4_SPI4
#define SPI_UNIT5           M4_SPI5
#define SPI_UNIT6           M4_SPI6

#define SPI1_CLK            PWC_FCG1_SPI1 
#define SPI2_CLK            PWC_FCG1_SPI2 
#define SPI3_CLK            PWC_FCG1_SPI3 
#define SPI4_CLK            PWC_FCG1_SPI4 
#define SPI5_CLK            PWC_FCG1_SPI5 
#define SPI6_CLK            PWC_FCG1_SPI6 

#define SPI_DMA_UNIT    M4_DMA1
#define SPI_DMA_RX_CH       (DMA_CH6)
#define SPI_DMA_TX_CH       (DMA_CH7)

typedef struct
{
    uint8_t ClkPort;
    uint16_t ClkPin;
    uint8_t Clkfunc;//RX功能
    uint8_t CsPort;
    uint16_t CsPin;
    uint8_t Csfunc;//TX功能
    uint8_t MosiPort;
    uint16_t MosiPin;
    uint8_t Mosifunc;//TX功能
    uint8_t MisoPort;
    uint16_t MisoPin;
    uint8_t Misofunc;//TX功能   
}bsp_spi_port_cfg_t;
typedef struct
{
    stc_spi_init_t stcSpiInit;//SPI参数
    bsp_spi_port_cfg_t *port;//管脚参数
}bsp_spi_cfg_t;
extern bsp_spi_port_cfg_t port_spi1,port_spi2,port_spi3,port_spi4,port_spi5,port_spi6;
#ifdef __cplusplus
extern "C" {
#endif
uint8_t bsp_spi4_init(void);
uint8_t bsp_spi_transmit8(M4_SPI_TypeDef *SPIx, uint8_t *txdata, uint8_t *rxdata, uint32_t len);
uint8_t bsp_spi_transmit8_Dma(M4_SPI_TypeDef *SPIx, uint8_t *txdata, uint8_t *rxdata, uint32_t len);
uint8_t bsp_set_spi_cs(M4_SPI_TypeDef *SPIx, bool value);
uint8_t bsp_spi_init(M4_SPI_TypeDef *SPIx,bsp_spi_cfg_t *spi_cfg);
#ifdef __cplusplus
};
#endif


#endif

