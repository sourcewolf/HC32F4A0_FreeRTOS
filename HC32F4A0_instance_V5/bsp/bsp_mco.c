#include "bsp_mco.h"


/* funcion name: bsp_mco_init
*功能：时钟输出初始化，MCO1,MCO2输出PLLAP的二分频
*输入参数：mco_num :暂时无效，请输入0，freq:暂时无效，请输入0
*输出参数：无
*/
void bsp_mco_init(uint8_t mco_num,	uint32_t freq)
{
	stc_gpio_init_t stcGpioInit;
	GPIO_StructInit(&stcGpioInit);
	stcGpioInit.u16PinDir = PIN_DIR_OUT;
	stcGpioInit.u16PinDrv = PIN_DRV_HIGH;
	stcGpioInit.u16Latch = PIN_LATCH_OFF;
	GPIO_Unlock();
	GPIO_SetSubFunc(GPIO_FUNC_1_MCO);
    GPIO_SetFunc(GPIO_PORT_E, GPIO_PIN_01, GPIO_FUNC_3_TIM6_TRIGC, PIN_SUBFUNC_ENABLE);
    GPIO_Init(MCO_PORT, MCO_PIN, &stcGpioInit);
	GPIO_SetFunc(MCO_PORT,MCO_PIN,GPIO_FUNC_1_MCO,PIN_SUBFUNC_DISABLE);
	GPIO_Lock();
	PWC_Unlock(PWC_UNLOCK_CODE_0);
	CLK_MCO1Config(CLK_MCOSOURCCE_PLLAP,CLK_MCO_DIV2);
	CLK_MCO1Cmd(Enable);
	CLK_MCO2Config(CLK_MCOSOURCCE_PLLAP,CLK_MCO_DIV2);
	CLK_MCO2Cmd(Enable);
	PWC_Lock(PWC_UNLOCK_CODE_0);
	
}

