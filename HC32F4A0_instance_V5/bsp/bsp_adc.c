#include "bsp_adc.h"
#include "bsp_interrupt.h"
/* funcion name: adc1_eoca_callback
*功能：ADC1 序列A转换完成回调函数
*输入参数：无
*输出参数：无
*/
void adc1_eoca_callback(void)
{
    
}
/* funcion name: bsp_adc_init
*功能：ADC1 初始化
*输入参数：无
*输出参数：无
*/
void bsp_adc_init(void)
{
    stc_gpio_init_t stcGpioInit;
    stc_adc_init_t stcInit;
    stc_irq_signin_config_t stcIrqRegiConf;
    /* Set a default value. */
	GPIO_StructInit(&stcGpioInit);
    ADC_StructInit(&stcInit);
    
	
    /* 2. Enable ADC peripheral clock. */
    PWC_Unlock(PWC_UNLOCK_CODE_1);
	PWC_Fcg3PeriphClockCmd(PWC_FCG3_ADC1, Enable);//开ADC1时钟
	PWC_Lock(PWC_UNLOCK_CODE_1);

    /* 3. Initializes ADC. */
    ADC_Init(M4_ADC1, &stcInit);
    
    uint8_t au8AdcSASplTime[] = {30,30,35,35};

    /* 4. Set the ADC pin to analog input mode. */
    stcGpioInit.u16PinAttr = PIN_ATTR_ANALOG;
	GPIO_Unlock();
	GPIO_Init(GPIO_PORT_A, GPIO_PIN_03, &stcGpioInit);
	GPIO_Lock();
    /* 5. Enable the ADC channels. */
    ADC_ChannelCmd(M4_ADC1, ADC_SEQ_A, \
                   (ADC_CH3 | ADC_CH4 | ADC_CH18 | ADC_CH19), au8AdcSASplTime, \
                   Enable);

    /* 6. Set the number of averaging sampling and enable the channel, if needed. */
#if (defined APP_ADC_AVG_CH) && (APP_ADC_AVG_CH != 0U)
    ADC_AvgChannelConfig(M4_ADC1, APP_ADC_AVG_CNT);
    ADC_AvgChannelCmd(M4_ADC1, APP_ADC_AVG_CH, Enable);
#endif
    
    ADC_SeqIntCmd(M4_ADC1,ADC_SEQ_A,Enable);//使能中断
    
//    stcIrqRegiConf.enIRQn = ADC_COA_IRQn;                    /* Register INT_TMR61_GOVF Int to Vect.No.002 */
//    stcIrqRegiConf.enIntSrc = INT_ADC1_EOCA;               /* Select Event interrupt of tmr61 */
//    stcIrqRegiConf.pfnCallback = &adc1_eoca_callback;   /* Callback function */
//    INTC_IrqSignIn(&stcIrqRegiConf);                        /* Registration IRQ */

//    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);            /* Clear Pending */
//    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);/* Set priority */
//    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);
    bsp_interrupt_callback_regist(INT_ADC1_EOCA,ADC_COA_IRQn,&adc1_eoca_callback);
//    bsp_share_interrupt_enable(INT_ADC1_EOCA);
}
/* funcion name: bsp_start_convert
*功能：ADC1 启动转换
*输入参数：无
*输出参数：无
*/
void bsp_start_convert(void)
{
    ADC_Start(M4_ADC1);
}