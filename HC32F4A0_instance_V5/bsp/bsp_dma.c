#include "bsp_dma.h"
/* function name: bsp_dma_init
*功能：初始化DMA
*输入参数:  M4_DMA_TypeDef* pstcDmaReg : M4_DMA1,M4_DMA2
            u8Ch: DMA_CH0 DMA_CH1 DMA_CH2 DMA_CH3 DMA_CH4 DMA_CH5 DMA_CH6 DMA_CH7
            enSrcMode: DMA_SRC_ADDR_FIX DMA_SRC_ADDR_INC DMA_SRC_ADDR_DEC
            enDesMode: DMA_DEST_ADDR_FIX DMA_DEST_ADDR_INC DMA_DEST_ADDR_DEC
            DataWidth: DMA_DATAWIDTH_8BIT DMA_DATAWIDTH_16BIT DMA_DATAWIDTH_32BIT
*输出参数：无
*/
void bsp_dma_init(M4_DMA_TypeDef* pstcDmaReg, \
                    uint8_t u8Ch,\
                    uint32_t enSrcMode,\
                    uint32_t enDesMode,\
                    uint32_t DataWidth
                )
{
    stc_dma_init_t stcDmaCfg;
    DMA_StructInit (&stcDmaCfg);
    
    stcDmaCfg.u32BlockSize = 1;//
    stcDmaCfg.u32TransCnt = 1;//
    stcDmaCfg.u32DestInc = enDesMode;
    stcDmaCfg.u32SrcInc = enSrcMode;
    stcDmaCfg.u32DataWidth = DataWidth;
    stcDmaCfg.u32IntEn = DMA_INT_ENABLE;
    /* Unlock PWC register: FCG0 */
    PWC_FCG0_Unlock();
    //turn on DMA UNIT Clock
    if(pstcDmaReg == M4_DMA1)
    {
       PWC_Fcg0PeriphClockCmd(PWC_FCG0_DMA1, Enable); 
    }
    else
    {
       PWC_Fcg0PeriphClockCmd(PWC_FCG0_DMA2, Enable); 
    }
    PWC_Fcg0PeriphClockCmd(PWC_FCG0_AOS, Enable);
    /* Lock PWC register: FCG0 */
    PWC_FCG0_Lock();
    /* Enable DMA UNIT. */
    DMA_Cmd(pstcDmaReg,Enable);   
    /* Initialize DMA channel. */
    DMA_Init(pstcDmaReg, u8Ch, &stcDmaCfg);
    /* Enable DMA channel. */
//    DMA_ChannelCmd(pstcDmaReg, u8Ch,Enable);
}
/* function name: bsp_dma_SetDesAddr
*功能：初始化DMA
*输入参数:  M4_DMA_TypeDef* pstcDmaReg : M4_DMA1,M4_DMA2
            u8Ch: DMA_CH0 DMA_CH1 DMA_CH2 DMA_CH3 DMA_CH4 DMA_CH5 DMA_CH6 DMA_CH7
            addr: u32_address
*输出参数：无
*/
void bsp_dma_SetDesAddr(M4_DMA_TypeDef* pstcDmaReg, \
                    uint8_t u8Ch,\
                    uint32_t addr
                    )
{
    DMA_SetDestAddr(pstcDmaReg, u8Ch, addr);
}
/* function name: bsp_dma_SetSrcAddr
*功能：初始化DMA
*输入参数:  M4_DMA_TypeDef* pstcDmaReg : M4_DMA1,M4_DMA2
            u8Ch: DMA_CH0 DMA_CH1 DMA_CH2 DMA_CH3 DMA_CH4 DMA_CH5 DMA_CH6 DMA_CH7
            addr: u32_address
*输出参数：无
*/
void bsp_dma_SetSrcAddr(M4_DMA_TypeDef* pstcDmaReg, \
                    uint8_t u8Ch,\
                    uint32_t addr
                    )
{
    DMA_SetSrcAddr(pstcDmaReg, u8Ch, addr);
}
/* function name: bsp_dma_init
*功能：初始化DMA
*输入参数:  M4_DMA_TypeDef* pstcDmaReg : M4_DMA1,M4_DMA2
            u8Ch: DMA_CH0 DMA_CH1 DMA_CH2 DMA_CH3 DMA_CH4 DMA_CH5 DMA_CH6 DMA_CH7
            EventSrc: en_event_src_t
*输出参数：无
*/
void bsp_dma_set_TrigSrc(M4_DMA_TypeDef* pstcDmaReg, \
                    uint8_t u8Ch,\
                    en_event_src_t EventSrc
                    )
{
    DMA_SetTriggerSrc(pstcDmaReg, u8Ch, EventSrc);
}
/* function name: bsp_dma_init
*功能：初始化DMA
*输入参数:  M4_DMA_TypeDef* pstcDmaReg : M4_DMA1,M4_DMA2
            u8Ch: DMA_CH0 DMA_CH1 DMA_CH2 DMA_CH3 DMA_CH4 DMA_CH5 DMA_CH6 DMA_CH7
            uint16_t
*输出参数：无
*/
void bsp_dma_set_count(M4_DMA_TypeDef* pstcDmaReg, \
                    uint8_t u8Ch,\
                    uint16_t num)
{
    DMA_SetTransCnt(pstcDmaReg, u8Ch, num);
}
/* function name: bsp_dma_init
*功能：初始化DMA
*输入参数:  M4_DMA_TypeDef* pstcDmaReg : M4_DMA1,M4_DMA2
            u8Ch: DMA_CH0 DMA_CH1 DMA_CH2 DMA_CH3 DMA_CH4 DMA_CH5 DMA_CH6 DMA_CH7
            value: Enable Disable
*输出参数：无
*/
void bsp_dma_ch_enable(M4_DMA_TypeDef* pstcDmaReg, \
                    uint8_t u8Ch, bool value)
{
    DMA_ChannelCmd(pstcDmaReg, u8Ch, value);
}



