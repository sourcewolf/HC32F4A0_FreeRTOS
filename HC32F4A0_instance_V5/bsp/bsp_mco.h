#ifndef BSP_MCO_H
#define BSP_MCO_H
#include "hc32_ddl.h"
#define MCO_PORT	(GPIO_PORT_F)
#define MCO_PIN		(GPIO_PIN_00)

#ifdef __cplusplus
extern "C" {
#endif
void bsp_mco_init(uint8_t mco_num,	uint32_t freq);
#ifdef __cplusplus
};
#endif
#endif

