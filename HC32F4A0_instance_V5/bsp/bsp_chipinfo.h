#ifndef BSP_CHIPINFO_H
#define BSP_CHIPINFO_H
#include "hc32_ddl.h"
typedef struct
{
    uint32_t CHIPID;
    stc_efm_unique_id_t uid;
}chipinfo_t;

#ifdef __cplusplus
extern "C" {
#endif

void bsp_get_chipinfo(void);

#ifdef __cplusplus
};
#endif

#endif
