#ifndef BSP_I2C_TYPE_H
#define BSP_I2C_TYPE_H
#include "hc32_ddl.h"

#define I2C1_UNIT M4_I2C1
#define I2C2_UNIT M4_I2C2
#define I2C3_UNIT M4_I2C3
#define I2C4_UNIT M4_I2C4
#define I2C5_UNIT M4_I2C5
#define I2C6_UNIT M4_I2C6

#if TESTI2C
#define I2C1_SCL_PORT   GPIO_PORT_B//GPIO_PORT_D//
#define I2C1_SCL_Pin    GPIO_PIN_08//GPIO_PIN_03//
#define I2C1_SCL_FUNC	GPIO_FUNC_51_I2C1_SCL//GPIO_FUNC_49_I2C1_SCL
#define I2C1_SDA_PORT   GPIO_PORT_B//GPIO_PORT_F//
#define I2C1_SDA_Pin    GPIO_PIN_09//GPIO_PIN_10//
#define I2C1_SDA_FUNC	GPIO_FUNC_50_I2C1_SDA//GPIO_FUNC_48_I2C1_SDA
#else
#define I2C1_SCL_PORT   GPIO_PORT_D//GPIO_PORT_B//
#define I2C1_SCL_Pin    GPIO_PIN_03//GPIO_PIN_08//
#define I2C1_SCL_FUNC	GPIO_FUNC_49_I2C1_SCL//GPIO_FUNC_51_I2C1_SCL//
#define I2C1_SDA_PORT   GPIO_PORT_F//GPIO_PORT_B//
#define I2C1_SDA_Pin    GPIO_PIN_10//GPIO_PIN_09//
#define I2C1_SDA_FUNC	GPIO_FUNC_48_I2C1_SDA//GPIO_FUNC_50_I2C1_SDA//
#endif
#define I2C1_CLK			PWC_FCG1_I2C1
#define I2C2_CLK			PWC_FCG1_I2C2
#define I2C3_CLK			PWC_FCG1_I2C3
#define I2C4_CLK			PWC_FCG1_I2C4
#define I2C5_CLK			PWC_FCG1_I2C5
#define I2C6_CLK			PWC_FCG1_I2C6

#define I2Cx_SCL_PORT	I2C1_SCL_PORT
#define I2Cx_SCL_Pin	I2C1_SCL_Pin
#define I2Cx_SCL_FUNC	I2C1_SCL_FUNC
#define I2Cx_SDA_PORT	I2C1_SDA_PORT
#define I2Cx_SDA_Pin	I2C1_SDA_Pin
#define I2Cx_SDA_FUNC	I2C1_SDA_FUNC
#define TIMEOUT                         ((uint32_t)0x50000)

#define I2C_RET_OK                      0
#define I2C_RET_ERROR                   1
#define I2C_BUSY						2
#define I2C_TIMEROUT					3
#define I2C_BADADDR						4
#define I2C_BADPARA                     5//��������

#define I2C_DMA_UNIT    M4_DMA1
#define DMA_RX_CH       (DMA_CH0)
#define DMA_TX_CH       (DMA_CH1)
#define DMA_RX_CH_TRIG  EVT_I2C1_RXI
#define DMA_TX_CH_TRIG  EVT_I2C1_TXI
#define DMA_TX_INT      INT_DMA1_TC1
#define DMA_RX_INT      INT_DMA1_TC0
#define DMA_Mode_RXsrc  DMA_SRC_ADDR_FIX
#define DMA_Mode_RXdes  DMA_DEST_ADDR_INC
#define DMA_Mode_TXsrc  DMA_SRC_ADDR_INC
#define DMA_Mode_TXdes  DMA_DEST_ADDR_FIX
#define DataWidth       DMA_DATAWIDTH_8BIT

#define SLAVE_DATA_LEN                   64u

#define SLAVE_I2C_DMA_UNIT    M4_DMA1
#define SLAVE_DMA_RX_CH       (DMA_CH2)
#define SLAVE_DMA_TX_CH       (DMA_CH3)
#define SLAVE_DMA_RX_CH_TRIG  EVT_I2C1_RXI
#define SLAVE_DMA_TX_CH_TRIG  EVT_I2C1_TXI
#define SLAVE_DMA_TX_INT      INT_DMA1_TC3
#define SLAVE_DMA_RX_INT      INT_DMA1_TC2
#define SLAVE_DMA_Mode_RXsrc  AddressFix
#define SLAVE_DMA_Mode_RXdes  AddressIncrease
#define SLAVE_DMA_Mode_TXsrc  AddressIncrease
#define SLAVE_DMA_Mode_TXdes  AddressFix
#define SLAVE_DataWidth       Dma8Bit

#define SLAVE_MODE_INT 0
#define SLAVE_MODE_DMA 1
#define SLAVE_MODE SLAVE_MODE_INT

#endif
