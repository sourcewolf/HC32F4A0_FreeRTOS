#include "bsp_trng.h"
/* funcion name: bsp_trng_init
*功能：初始化真随机数单元
*输入参数：无
*输出参数：无
*/
void bsp_trng_init(void)
{
	PWC_FCG0_Unlock();
	PWC_Fcg0PeriphClockCmd(PWC_FCG0_TRNG, Enable);
	PWC_FCG0_Lock();
	/* Shift 64 times */
    TRNG_SetShiftCnt(TRNG_SHIFT_COUNT_64);
    /* Disable the Load function */
    TRNG_ReloadCmd(TRNG_RELOAD_DISABLE);
}
/* funcion name: bsp_trng_gen
*功能：产生随机数
*输入参数：u32TrngArr 随机数结果，32位数组，结果为64随机数，两个32位， count 产生随机数个数，32位
*输出参数：无
*/
void bsp_trng_gen(uint32_t *u32TrngArr,uint32_t count)
{
	uint32_t i = 0U,trngloop = count / 2U;

	for(i = 0U; i < trngloop; i++)
    {
        TRNG_Generate(&u32TrngArr[i * 2U]);
    }
}








