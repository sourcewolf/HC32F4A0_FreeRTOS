#ifndef BSP_RMU_H
#define BSP_RMU_H
#include "hc32_ddl.h"
#ifdef __cplusplus
extern "C" {
#endif
uint32_t bsp_get_resetcause(void);
void bsp_show_resetcause(uint32_t resetflag);
#ifdef __cplusplus
};
#endif


#endif


