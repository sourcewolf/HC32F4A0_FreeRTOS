#include "bsp_efm.h"
#include "bsp_gpio.h"
#include "bsp_log.h"
#define APP_START_ADDRESS  0x100000
EFM_data_t flashdata;
void cacheon(void)
{
    EFM_Unlock();
    EFM_DataCacheCmd(Enable);
    EFM_InsCacheCmd(Enable);
    EFM_PrefetchCmd(Enable);
    EFM_Lock();
}
void cacheoff(void)
{
    EFM_Unlock();
    EFM_DataCacheCmd(Disable);
    EFM_InsCacheCmd(Disable);
    EFM_PrefetchCmd(Disable);
    EFM_Lock();
}
void bsp_efm_read_8(uint8_t *readbuf, uint32_t addr, uint32_t len)
{
    uint8_t * desdata;
    desdata = (uint8_t*)addr;
    for(uint32_t i=0;i<len;i++)
    {
        readbuf[i] = desdata[i];
    }
}
    
void bsp_efm_read_16(uint16_t *readbuf, uint32_t addr, uint32_t len)
{
    uint16_t * desdata;
    desdata = (uint16_t*)addr;
    for(uint32_t i=0;i<len;i++)
    {
        readbuf[i] = desdata[i];
    }
}

void bsp_efm_read_32(uint32_t *readbuf, uint32_t addr, uint32_t len)
{
    uint32_t * desdata;
    desdata = (uint32_t*)addr;
    for(uint32_t i=0;i<len;i++)
    {
        readbuf[i] = desdata[i];
    }
}

void bsp_efm_write(uint32_t *writebuf, uint32_t addr, uint32_t len)
{
    stc_efm_cfg_t stcEfmCfg;
    en_int_status_t flag1;
    en_int_status_t flag2;
    uint32_t sectoraddr = addr&0xFFFFE000;//ȡҳ��ַ
    uint32_t sectornum = sectoraddr/0x2000;//ȡҳ���
    EFM_Unlock();
    EFM_FWMC_Unlock();
    EFM_StructInit(&stcEfmCfg);
    stcEfmCfg.u32WaitCycle = EFM_WAIT_CYCLE_5;
    EFM_Init(&stcEfmCfg);
    /* Wait flash0, flash1 ready. */
    do{
        flag1 = EFM_GetFlagStatus(EFM_FLAG_RDY0);
        flag2 = EFM_GetFlagStatus(EFM_FLAG_RDY1);
    }while((Set != flag1) || (Set != flag2));
    /* Sector disables write protection */
    (void)EFM_SectorCmd_Single(sectornum, Enable);
    /* Erase sector */
    (void)EFM_SectorErase(sectoraddr);
    for(int i=0;i<len;i++)
    {
        EFM_SingleProgram(addr,*writebuf);
        addr+=4;
        writebuf++;
    }    
    EFM_FWMC_Lock();
    EFM_Lock();
} 
void bsp_testefm(void)
{    
    bsp_efm_read_32(flashdata.data32,0,2048);
    bsp_led_toggle();
    bsp_efm_write(flashdata.data32,0x100000,2048);
    bsp_led_toggle();
}
void bsp_swapflash(originbankslect_t selectedbank)
{
    EFM_Unlock();
    EFM_FWMC_Unlock();
    EFM_SwapCmd((bool)selectedbank);
    EFM_FWMC_Lock();
    EFM_Lock();
}
originbankslect_t bsp_getstartbank(void)
{
    originbankslect_t status = (bool)(M4_EFM->FSWP & 0x01);
    return status;
}
void bsp_showCurrntEFMbank(void)
{
    if( bsp_getstartbank() == STARTFROM_BANK0)
    {
        LOG_INFO("-------------start from bank0!!!----------\r\n");
    }
    else
    {
        LOG_INFO("-------------start from bank1!!!----------\r\n");
    }
}
void bsp_test_copybank02bank1(void)
{
    for(int i = 0; i<11; i++)
    {
        bsp_efm_read_32(flashdata.data32,i*0x2000,2048);
        bsp_efm_write(flashdata.data32,i*0x2000+0x100000,2048);
    }
    LOG_INFO("-------------Copy complete!!!----------\r\n");
}
static int bsp_jumpbacktobank0(void)
{
    uint32_t app_start_address;
	/* Load the Reset Handler address of the application */
	app_start_address = *(uint32_t *)(APP_START_ADDRESS + 4);

	/**
	 * Test reset vector of application @APP_START_ADDRESS+4
	 * Stay in SAM-BA if *(APP_START+0x4) == 0xFFFFFFFF
	 * Application erased condition
	 */
	if (app_start_address == 0xFFFFFFFF) {
		return 1;
	}
	/* Rebase the Stack Pointer */
	__set_MSP(*(uint32_t *) APP_START_ADDRESS);

	/* Rebase the vector table base address */
	SCB->VTOR = ((uint32_t) APP_START_ADDRESS & SCB_VTOR_TBLOFF_Msk);

	/* Jump to application Reset Handler in the application */
    (*((void(*)(void))app_start_address))();
	return 0;
}
void calljumpapp(void)
{
    bsp_jumpbacktobank0();
}