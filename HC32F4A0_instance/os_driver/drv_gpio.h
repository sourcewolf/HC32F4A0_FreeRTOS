#ifndef DRV_GPIO_H
#define DRV_GPIO_H
#include "hc32_ddl.h"

#ifndef RT_Thread
#define RT_NULL NULL
#endif

#define PIN_MODE_OUTPUT         0x00
#define PIN_MODE_INPUT          0x01
#define PIN_MODE_INPUT_PULLUP   0x02
#define PIN_MODE_INPUT_PULLDOWN 0x03
#define PIN_MODE_OUTPUT_OD      0x04

#define __HC32_PIN(index, gpio, gpio_index)                                \
    {                                                                       \
        index, GPIO_PORT_##gpio, GPIO_PIN_##gpio_index                            \
    }
#define __HC32_PIN_RESERVE                                                 \
    {                                                                       \
        -1, 0, 0                                                            \
    }

#define ITEM_NUM(items) sizeof(items) / sizeof(items[0])

struct pin_index
{
    int index;
    uint8_t gpio;
    uint32_t pin;
};

struct pin_irq_map
{
    uint16_t pinbit;
    IRQn_Type irqno;
};
static const struct pin_index pins[] = 
{
#if defined(GPIO_PORT_A)
    __HC32_PIN(0 ,  A, 00 ),
    __HC32_PIN(1 ,  A, 01 ),
    __HC32_PIN(2 ,  A, 02 ),
    __HC32_PIN(3 ,  A, 03 ),
    __HC32_PIN(4 ,  A, 04 ),
    __HC32_PIN(5 ,  A, 05 ),
    __HC32_PIN(6 ,  A, 06 ),
    __HC32_PIN(7 ,  A, 07 ),
    __HC32_PIN(8 ,  A, 08 ),
    __HC32_PIN(9 ,  A, 09 ),
    __HC32_PIN(10,  A, 10),
    __HC32_PIN(11,  A, 11),
    __HC32_PIN(12,  A, 12),
    __HC32_PIN(13,  A, 13),
    __HC32_PIN(14,  A, 14),
    __HC32_PIN(15,  A, 15),
#if defined(GPIO_PORT_B)
    __HC32_PIN(16,  B, 00),
    __HC32_PIN(17,  B, 01),
    __HC32_PIN(18,  B, 02),
    __HC32_PIN(19,  B, 03),
    __HC32_PIN(20,  B, 04),
    __HC32_PIN(21,  B, 05),
    __HC32_PIN(22,  B, 06),
    __HC32_PIN(23,  B, 07),
    __HC32_PIN(24,  B, 08),
    __HC32_PIN(25,  B, 09),
    __HC32_PIN(26,  B, 10),
    __HC32_PIN(27,  B, 11),
    __HC32_PIN(28,  B, 12),
    __HC32_PIN(29,  B, 13),
    __HC32_PIN(30,  B, 14),
    __HC32_PIN(31,  B, 15),
#if defined(GPIO_PORT_C)
    __HC32_PIN(32,  C, 00),
    __HC32_PIN(33,  C, 01),
    __HC32_PIN(34,  C, 02),
    __HC32_PIN(35,  C, 03),
    __HC32_PIN(36,  C, 04),
    __HC32_PIN(37,  C, 05),
    __HC32_PIN(38,  C, 06),
    __HC32_PIN(39,  C, 07),
    __HC32_PIN(40,  C, 08),
    __HC32_PIN(41,  C, 09),
    __HC32_PIN(42,  C, 10),
    __HC32_PIN(43,  C, 11),
    __HC32_PIN(44,  C, 12),
    __HC32_PIN(45,  C, 13),
    __HC32_PIN(46,  C, 14),
    __HC32_PIN(47,  C, 15),
#if defined(GPIO_PORT_D)
    __HC32_PIN(48,  D, 00),
    __HC32_PIN(49,  D, 01),
    __HC32_PIN(50,  D, 02),
    __HC32_PIN(51,  D, 03),
    __HC32_PIN(52,  D, 04),
    __HC32_PIN(53,  D, 05),
    __HC32_PIN(54,  D, 06),
    __HC32_PIN(55,  D, 07),
    __HC32_PIN(56,  D, 08),
    __HC32_PIN(57,  D, 09),
    __HC32_PIN(58,  D, 10),
    __HC32_PIN(59,  D, 11),
    __HC32_PIN(60,  D, 12),
    __HC32_PIN(61,  D, 13),
    __HC32_PIN(62,  D, 14),
    __HC32_PIN(63,  D, 15),
#if defined(GPIO_PORT_E)
    __HC32_PIN(64,  E, 00),
    __HC32_PIN(65,  E, 01),
    __HC32_PIN(66,  E, 02),
    __HC32_PIN(67,  E, 03),
    __HC32_PIN(68,  E, 04),
    __HC32_PIN(69,  E, 05),
    __HC32_PIN(70,  E, 06),
    __HC32_PIN(71,  E, 07),
    __HC32_PIN(72,  E, 08),
    __HC32_PIN(73,  E, 09),
    __HC32_PIN(74,  E, 10),
    __HC32_PIN(75,  E, 11),
    __HC32_PIN(76,  E, 12),
    __HC32_PIN(77,  E, 13),
    __HC32_PIN(78,  E, 14),
    __HC32_PIN(79,  E, 15),
#if defined(GPIO_PORT_F)
    __HC32_PIN(80,  F, 00),
    __HC32_PIN(81,  F, 01),
    __HC32_PIN(82,  F, 02),
    __HC32_PIN(83,  F, 03),
    __HC32_PIN(84,  F, 04),
    __HC32_PIN(85,  F, 05),
    __HC32_PIN(86,  F, 06),
    __HC32_PIN(87,  F, 07),
    __HC32_PIN(88,  F, 08),
    __HC32_PIN(89,  F, 09),
    __HC32_PIN(90,  F, 10),
    __HC32_PIN(91,  F, 11),
    __HC32_PIN(92,  F, 12),
    __HC32_PIN(93,  F, 13),
    __HC32_PIN(94,  F, 14),
    __HC32_PIN(95,  F, 15),
#if defined(GPIO_PORT_G)
    __HC32_PIN(96,  G, 00),
    __HC32_PIN(97,  G, 01),
    __HC32_PIN(98,  G, 02),
    __HC32_PIN(99,  G, 03),
    __HC32_PIN(100, G, 04),
    __HC32_PIN(101, G, 05),
    __HC32_PIN(102, G, 06),
    __HC32_PIN(103, G, 07),
    __HC32_PIN(104, G, 08),
    __HC32_PIN(105, G, 09),
    __HC32_PIN(106, G, 10),
    __HC32_PIN(107, G, 11),
    __HC32_PIN(108, G, 12),
    __HC32_PIN(109, G, 13),
    __HC32_PIN(110, G, 14),
    __HC32_PIN(111, G, 15),
#if defined(GPIO_PORT_H)
    __HC32_PIN(112, H, 00),
    __HC32_PIN(113, H, 01),
    __HC32_PIN(114, H, 02),
    __HC32_PIN(115, H, 03),
    __HC32_PIN(116, H, 04),
    __HC32_PIN(117, H, 05),
    __HC32_PIN(118, H, 06),
    __HC32_PIN(119, H, 07),
    __HC32_PIN(120, H, 08),
    __HC32_PIN(121, H, 09),
    __HC32_PIN(122, H, 10),
    __HC32_PIN(123, H, 11),
    __HC32_PIN(124, H, 12),
    __HC32_PIN(125, H, 13),
    __HC32_PIN(126, H, 14),
    __HC32_PIN(127, H, 15),
#if defined(GPIO_PORT_I)
    __HC32_PIN(128, I, 00),
    __HC32_PIN(129, I, 01),
    __HC32_PIN(130, I, 02),
    __HC32_PIN(131, I, 03),
    __HC32_PIN(132, I, 04),
    __HC32_PIN(133, I, 05),
    __HC32_PIN(134, I, 06),
    __HC32_PIN(135, I, 07),
    __HC32_PIN(136, I, 08),
    __HC32_PIN(137, I, 09),
    __HC32_PIN(138, I, 10),
    __HC32_PIN(139, I, 11),
    __HC32_PIN(140, I, 12),
    __HC32_PIN(141, I, 13),
    __HC32_PIN(142, I, 14),
    __HC32_PIN(143, I, 15),
#if defined(GPIO_PORT_J)
    __HC32_PIN(144, J, 0),
    __HC32_PIN(145, J, 1),
    __HC32_PIN(146, J, 2),
    __HC32_PIN(147, J, 3),
    __HC32_PIN(148, J, 4),
    __HC32_PIN(149, J, 5),
    __HC32_PIN(150, J, 6),
    __HC32_PIN(151, J, 7),
    __HC32_PIN(152, J, 8),
    __HC32_PIN(153, J, 9),
    __HC32_PIN(154, J, 10),
    __HC32_PIN(155, J, 11),
    __HC32_PIN(156, J, 12),
    __HC32_PIN(157, J, 13),
    __HC32_PIN(158, J, 14),
    __HC32_PIN(159, J, 15),
#if defined(GPIO_PORT_K)
    __HC32_PIN(160, K, 0),
    __HC32_PIN(161, K, 1),
    __HC32_PIN(162, K, 2),
    __HC32_PIN(163, K, 3),
    __HC32_PIN(164, K, 4),
    __HC32_PIN(165, K, 5),
    __HC32_PIN(166, K, 6),
    __HC32_PIN(167, K, 7),
    __HC32_PIN(168, K, 8),
    __HC32_PIN(169, K, 9),
    __HC32_PIN(170, K, 10),
    __HC32_PIN(171, K, 11),
    __HC32_PIN(172, K, 12),
    __HC32_PIN(173, K, 13),
    __HC32_PIN(174, K, 14),
    __HC32_PIN(175, K, 15),
#endif /* defined(GPIOK) */
#endif /* defined(GPIOJ) */
#endif /* defined(GPIOI) */
#endif /* defined(GPIOH) */
#endif /* defined(GPIOG) */
#endif /* defined(GPIOF) */
#endif /* defined(GPIOE) */
#endif /* defined(GPIOD) */
#endif /* defined(GPIOC) */
#endif /* defined(GPIOB) */
#endif /* defined(GPIOA) */
};
static const struct pin_irq_map pin_irq_map[] =
{
#if defined(HC32F4A0) || defined(HC32F460)
    {GPIO_PIN_00, Int128_IRQn},//share Interrupt
    {GPIO_PIN_01, Int128_IRQn},
    {GPIO_PIN_02, Int128_IRQn},
    {GPIO_PIN_03, Int128_IRQn},
    {GPIO_PIN_04, Int128_IRQn},
    {GPIO_PIN_05, Int128_IRQn},
    {GPIO_PIN_06, Int128_IRQn},
    {GPIO_PIN_07, Int128_IRQn},
    {GPIO_PIN_08, Int128_IRQn},
    {GPIO_PIN_09, Int128_IRQn},
    {GPIO_PIN_10, Int128_IRQn},
    {GPIO_PIN_11, Int128_IRQn},
    {GPIO_PIN_12, Int128_IRQn},
    {GPIO_PIN_13, Int128_IRQn},
    {GPIO_PIN_14, Int128_IRQn},
    {GPIO_PIN_15, Int128_IRQn}, 
#else
    {GPIO_PIN_0, EXTI0_IRQn},
    {GPIO_PIN_1, EXTI1_IRQn},
    {GPIO_PIN_2, EXTI2_IRQn},
    {GPIO_PIN_3, EXTI3_IRQn},
    {GPIO_PIN_4, EXTI4_IRQn},
    {GPIO_PIN_5, EXTI9_5_IRQn},
    {GPIO_PIN_6, EXTI9_5_IRQn},
    {GPIO_PIN_7, EXTI9_5_IRQn},
    {GPIO_PIN_8, EXTI9_5_IRQn},
    {GPIO_PIN_9, EXTI9_5_IRQn},
    {GPIO_PIN_10, EXTI15_10_IRQn},
    {GPIO_PIN_11, EXTI15_10_IRQn},
    {GPIO_PIN_12, EXTI15_10_IRQn},
    {GPIO_PIN_13, EXTI15_10_IRQn},
    {GPIO_PIN_14, EXTI15_10_IRQn},
    {GPIO_PIN_15, EXTI15_10_IRQn},
#endif
};



#ifdef __cplusplus
extern "C" {
#endif
extern const struct pin_index *get_pin(uint8_t pin);
extern void hc32_pin_mode(uint8_t pin, uint32_t mode);
extern void hc32_pin_write(uint8_t pin, uint8_t value);
extern int hc32_pin_read(uint8_t pin);
extern void hc32_pin_func_set(uint8_t pin, uint8_t func);
#ifdef __cplusplus
}
#endif

#endif