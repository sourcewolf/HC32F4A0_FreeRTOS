#include"drv_i2c.h"
//example
/*
hc32_i2c_obj_t obj_i2c1 = 
{
	.pstcI2Cx = I2C1_UNIT,
	.scl_pin = 0,
	.scl_pin_func = GPIO_FUNC_49_I2C1_SCL,
	.sda_pin = 1,
	.sda_pin_func = GPIO_FUNC_48_I2C1_SDA,
	.baudrate = 400000,
//	.timerout = 0x50000;
    
};
*/
/* funcion name: drv_hc32_i2c_init
*功能：I2C 设备初始化
*输入参数：*obj I2C对象，类型hc32_i2c_obj_t结构体
*输出参数：无
*/
void drv_hc32_i2c_init(hc32_i2c_obj_t *obj)
{
    //设置IO为I2C功能
	hc32_pin_func_set(obj->scl_pin,obj->scl_pin_func);
    hc32_pin_func_set(obj->sda_pin,obj->sda_pin_func);
    //初始化I2C
    bsp_i2c_init(obj->pstcI2Cx,obj->baudrate);
}
/* funcion name: drv_hc32_i2c_deinit
*功能：I2C 设备反初始化
*输入参数：*obj I2C对象，类型hc32_i2c_obj_t结构体
*输出参数：无
*/
void drv_hc32_i2c_deinit(hc32_i2c_obj_t *obj)
{
    //反初始化I2C
    bsp_i2c_deinit(obj->pstcI2Cx);
    //将IO还原成GPIO功能
    hc32_pin_func_set(obj->scl_pin,GPIO_FUNC_0_GPO);
    hc32_pin_func_set(obj->sda_pin,GPIO_FUNC_0_GPO);
}
/* funcion name: I2C_MasterTransmit
*功能：I2C 发送
*输入参数：addr,器件地址，*data 数据指针， num 数据长度， *obj I2C对象
*输出参数：无
*/
int32_t I2C_MasterTransmit (uint32_t addr, const uint8_t *data, uint32_t num, hc32_i2c_obj_t *obj)
{
    return I2C_Write_Buffer(obj->pstcI2Cx,addr,data,num);
}
/* funcion name: I2C_MasterReceive
*功能：I2C 接收
*输入参数：addr,器件地址，*data 数据指针， num 数据长度， *obj I2C对象
*输出参数：无
*/
int32_t I2C_MasterReceive (uint32_t addr, uint8_t *data, uint32_t num, hc32_i2c_obj_t *obj) 
{
    return I2C_Read_Buffer(obj->pstcI2Cx,addr,data,num);
}
