#ifndef DRV_I2C_H
#define DRV_I2C_H
#include "hc32_ddl.h"
#include "drv_gpio.h"
#include "bsp_i2c.h"
typedef struct hc32_i2c_obj
{
	M4_I2C_TypeDef *pstcI2Cx;
	uint8_t scl_pin;
    uint8_t scl_pin_func;
	uint8_t sda_pin;
    uint8_t sda_pin_func;
	uint32_t baudrate;
//	uint32_t timerout;
}hc32_i2c_obj_t;
#ifdef __cplusplus
extern "C" {
#endif
extern void drv_hc32_i2c_init(hc32_i2c_obj_t *obj);
extern int32_t I2C_MasterTransmit (uint32_t addr, const uint8_t *data, uint32_t num, hc32_i2c_obj_t *obj);
extern int32_t I2C_MasterReceive (uint32_t addr, uint8_t *data, uint32_t num, hc32_i2c_obj_t *obj);
#ifdef __cplusplus
}
#endif


#endif
