#ifndef BSP_INTERRUPT_H
#define BSP_INTERRUPT_H
#include "hc32_ddl.h"
#include "bsp_irqn.h"


#ifdef __cplusplus
extern "C" {
#endif
void bsp_interrupt_callback_regist(en_int_src_t enIntSrc, IRQn_Type enIRQn, void *callback);
void bsp_share_interrupt_enable(en_int_src_t enIntSrc);
void bsp_interrupt_enable(IRQn_Type enIRQn, bool value);
void bsp_set_interrupt_priority(IRQn_Type IRQn,uint32_t priority);
#ifdef __cplusplus
};
#endif    
    
#endif