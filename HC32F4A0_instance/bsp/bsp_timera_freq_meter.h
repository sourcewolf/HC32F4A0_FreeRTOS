#ifndef BSP_TIMA_FREQ_H
#define BSP_TIMA_FREQ_H
#include "hc32_ddl.h"
#include "bsp_irqn.h"
#ifdef __cplusplus
extern "C" {
#endif
void bsp_timerA_timer_1ms_init(void);
void bsp_timerA_counter_init(void);
#ifdef __cplusplus
};
#endif
#endif

