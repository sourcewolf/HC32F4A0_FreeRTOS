#ifndef BSP_GPIO_H
#define BSP_GPIO_H
#include "hc32_ddl.h"




#ifdef __cplusplus
extern "C" {
#endif

void bsp_gpio_init(void);
void bsp_led_toggle(void);

#ifdef __cplusplus
};
#endif

#endif


