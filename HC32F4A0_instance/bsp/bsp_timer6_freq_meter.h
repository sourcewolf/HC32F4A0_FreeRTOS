#ifndef BSP_TIM6_FREQ_H
#define BSP_TIM6_FREQ_H
#include "hc32_ddl.h"
#include "bsp_irqn.h"
#define CNT_TMR_UNIT	(M4_TMR6_3)//����TMER
#define T_TMR_UNIT		(M4_TMR6_2)//��ʱTIMER
#define FRQ_IN_PORT	(GPIO_PORT_E)//TRIG_C
#define FRQ_IN_PIN	(GPIO_PIN_01)
#define CNT_TMR_EN	(PWC_FCG2_TMR6_3)
#define CNT_CAPT_INT	(INT_TMR6_3_GCMA)
#define CNT_OVERFLOW_INT	(INT_TMR6_3_GOVF)
#define T_TMR_EN	(PWC_FCG2_TMR6_2)
#define T_TMR_INT	(INT_TMR6_2_GOVF)

#define PORT_FREQ_IN	(GPIO_PORT_F)
#define PIN_FREQ_IN		(GPIO_PIN_12)
#define EXITCH_FREQ		EXINT_CH12
#define INT_FREQ		INT_SWI_IRQ12
#define EVENT_FREQ		EVT_SWI_IRQ12


#ifdef __cplusplus
extern "C" {
#endif

void bsp_timer6_counter_init(void);
void bsp_timer6_timer_1ms_init(void);
void ExtPort_Init(void);

#ifdef __cplusplus
};
#endif

#endif


