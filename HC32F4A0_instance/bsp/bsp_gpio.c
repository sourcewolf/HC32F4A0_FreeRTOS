#include "bsp_gpio.h"
/* function name: bsp_gpio_init
*功能：初始化PC09为输出
*输入参数：无
*输出参数：无
*/
void bsp_gpio_init(void)
{
	stc_gpio_init_t stcGpioInit;
	GPIO_StructInit(&stcGpioInit);
	stcGpioInit.u16PinDir = PIN_DIR_OUT;
	GPIO_Unlock();
	GPIO_Init(GPIO_PORT_C, GPIO_PIN_09, &stcGpioInit);
	GPIO_Lock();
}
/* function name: bsp_led_toggle
*功能：翻转PC09
*输入参数：无
*输出参数：无
*/
void bsp_led_toggle(void)
{
	GPIO_TogglePins(GPIO_PORT_C, GPIO_PIN_09);
}
