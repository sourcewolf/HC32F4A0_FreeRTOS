#include "bsp_timer0.h"
#include "bsp_interrupt.h"
#include "bsp_log.h"
static void bsp_timer0_cha_callback(void)
{
    LOGERR("Tick %d",SysTick_GetTick());
	LOG_INFO("bsp_timer0_cha_callback");
}
void bsp_timer0_init_interrupt(void)
{
	stc_tmr0_init_t stcTmr0Init;
	stc_irq_signin_config_t stcIrqRegiConf;
	TMR0_StructInit(&stcTmr0Init);
	PWC_Unlock(PWC_UNLOCK_CODE_1);
	PWC_Fcg2PeriphClockCmd(PWC_FCG2_TMR0_1, Enable);//开TIMER6时钟
	PWC_Lock(PWC_UNLOCK_CODE_1);
	 stcTmr0Init.u32ClockDivision = TMR0_CLK_DIV16;        /* Config clock division */
    stcTmr0Init.u32ClockSource = TMR0_CLK_SRC_LRC;//TMR0_CLK_SRC_PCLK1;          /* Chose clock source */
    stcTmr0Init.u32Tmr0Func = TMR0_FUNC_CMP;            /* Timer0 compare mode */
    stcTmr0Init.u16CmpValue = 1023U;             /* Set compara register data */
    TMR0_Init(M4_TMR0_1, TMR0_CH_A, &stcTmr0Init);
    /* In asynchronous clock, If you want to write a TMR0 register, you need to wait for at 
       least 3 asynchronous clock cycles after the last write operation! */
    DDL_DelayMS(1U); /* Wait at least 3 asynchronous clock cycles.*/
    /* Timer0 interrupt function Enable */
    TMR0_IntCmd(M4_TMR0_1, TMR0_CH_A, Enable);
	DDL_DelayMS(1U);
    bsp_interrupt_callback_regist(INT_TMR0_1_CMPA,TIMER01_CHA_IRQn,bsp_timer0_cha_callback);
    bsp_set_interrupt_priority(TIMER01_CHA_IRQn,DDL_IRQ_PRIORITY_14);    
	TMR0_Cmd(M4_TMR0_1, TMR0_CH_A, Enable);
}
uint8_t bsp_timer0_init(M4_TMR0_TypeDef *Timer0x,uint8_t enCh,stc_tmr0_init_t* pstcBaseInit,uint8_t RTOMODE_en)
{
	stc_irq_signin_config_t stcIrqRegiConf;
	PWC_Unlock(PWC_UNLOCK_CODE_1);
	PWC_Fcg2PeriphClockCmd(PWC_FCG2_TMR0_1, Enable);//开TIMER6时钟
    PWC_Lock(PWC_UNLOCK_CODE_1);
    TMR0_Init(M4_TMR0_1, TMR0_CH_A, pstcBaseInit);
    DDL_DelayMS(1U); /* Wait at least 3 asynchronous clock cycles.*/
    if(RTOMODE_en)
    {
        
        TMR0_HWTrigCmd(Timer0x,enCh,TMR0_BT_HWTRG_FUNC_CLEAR|TMR0_BT_HWTRG_FUNC_START,Enable);
    }
    TMR0_SetCntVal(Timer0x, enCh,0);//计数清零
    return Ok;
}


uint8_t bsp_timer0_start(M4_TMR0_TypeDef *Timer0x,uint8_t enCh)
{
    TMR0_Cmd(Timer0x,enCh,Enable);
    return Ok;
}
uint8_t bsp_timer0_stop(M4_TMR0_TypeDef *Timer0x,uint8_t enCh)
{
    TMR0_Cmd(Timer0x,enCh,Disable);
    return Ok;
}