#ifndef BSP_ADC_H
#define BSP_ADC_H
#include "hc32_ddl.h"
#include "bsp_irqn.h"

#ifdef __cplusplus
extern "C" {
#endif
void bsp_adc_init(void);
void bsp_start_convert(void);
#ifdef __cplusplus
};
#endif
#endif
