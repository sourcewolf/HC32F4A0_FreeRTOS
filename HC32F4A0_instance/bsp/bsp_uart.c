#include "bsp_uart.h"
#include "bsp_log.h"
static uint8_t uart1_rxpos;
uint8_t UART1_RXbuff[RXLEN],UART2_RXbuff[RXLEN],UART3_RXbuff[RXLEN],UART4_RXbuff[RXLEN];
uint8_t bsp_usart_gpio_init(bsp_uart_cfg_t *uartcfg)
{
    GPIO_Unlock();
    GPIO_SetFunc(uartcfg->rxPort,uartcfg->rxPin,uartcfg->rxfunc,Disable);
    GPIO_SetFunc(uartcfg->txPort,uartcfg->txPin,uartcfg->txfunc,Disable);
    GPIO_Lock();
    return Ok;
}
uint8_t bsp_usart2_gpio_init(void)
{
    return Ok;
}
static void usart1_rx_callback(void)
{    
    UART1_RXbuff[uart1_rxpos] = USART_RecData(M4_USART1);
    USART_SendData(M4_USART1,UART1_RXbuff[uart1_rxpos]);
    uart1_rxpos++;
    if(uart1_rxpos>=RXLEN)
    {
        uart1_rxpos = 0;
    }
}
static void usart1_dma_callback(void)
{
//    bsp_timer0_stop(M4_TMR0_1,TMR0_CH_A);
//    bsp_dma_SetDesAddr(UART1_RX_DMA,UART1_RX_CH,(uint32_t)UART1_RXbuff);
//    bsp_dma_set_count(UART1_RX_DMA,UART1_RX_CH,RXLEN);
//    bsp_dma_ch_enable(UART1_RX_DMA,UART1_RX_CH,Enable);   
}
static void usart1_rx_timerout_callback(void)
{
    bsp_timer0_stop(M4_TMR0_1,TMR0_CH_A);//TIMER OUT后必须关闭时钟，否则会导致一直产生超时中断。
    USART_ClearStatus(M4_USART1,USART_CLEAR_FLAG_RTOF);
#ifdef UART_RXDMA_EN   
    bsp_dma_ch_enable(UART1_RX_DMA,UART1_RX_CH,Disable); 
    bsp_timer0_stop(M4_TMR0_1,TMR0_CH_A);
    bsp_dma_SetDesAddr(UART1_RX_DMA,UART1_RX_CH,(uint32_t)UART1_RXbuff);
    bsp_dma_set_count(UART1_RX_DMA,UART1_RX_CH,RXLEN);
    bsp_dma_ch_enable(UART1_RX_DMA,UART1_RX_CH,Enable);
#else
    uart1_rxpos =0;    
#endif    
}
static void usart1_error_callback(void)
{
    LOGERR("uart1 error M4_USART1->SR = %d",M4_USART1->SR);
    if(Set == USART_GetStatus(M4_USART1,USART_FLAG_ORE))
    {
        USART_ClearStatus(M4_USART1,USART_CLEAR_FLAG_ORE);
    }
    if(Set == USART_GetStatus(M4_USART1,USART_FLAG_FE))
    {
        USART_ClearStatus(M4_USART1,USART_CLEAR_FLAG_FE);
    }
    if(Set == USART_GetStatus(M4_USART1,USART_FLAG_PE))
    {
        USART_ClearStatus(M4_USART1,USART_CLEAR_FLAG_PE);
    }
}
static void bsp_uart1_cfg(M4_USART_TypeDef *USARTx,bsp_uart_cfg_t *uart_cfg)
{
    stc_tmr0_init_t stcTimerCfg;
    TMR0_StructInit(&stcTimerCfg);
    stcTimerCfg.u32ClockSource = TMR0_CLK_SRC_PCLK1;
    stcTimerCfg.u32Tmr0Func = TMR0_FUNC_CMP;
    stcTimerCfg.u32ClockDivision = TMR0_CLK_DIV4;
    stcTimerCfg.u16CmpValue = 6000-1;
    stcTimerCfg.u16CntValue = 0;    
    bsp_interrupt_callback_regist(INT_USART1_EI,USART1_ER_IRQn,uart_cfg->uart_err_callback);
    if(uart_cfg->RTO_enable == Enable)
    {
        bsp_timer0_init(M4_TMR0_1,TMR0_CH_A,&stcTimerCfg,uart_cfg->RTO_enable);
        bsp_interrupt_callback_regist(INT_USART1_RTO,USART1_RTO_IRQn,uart_cfg->uart_timeout_callback);
    }
#ifdef UART_RXDMA_EN
    bsp_dma_init(uart_cfg->DmaUnit,uart_cfg->DmaCh,DMA_SRC_ADDR_FIX,DMA_DEST_ADDR_INC,DMA_DATAWIDTH_8BIT);
    bsp_dma_set_count(uart_cfg->DmaUnit,uart_cfg->DmaCh,RXLEN);
    bsp_dma_set_TrigSrc(uart_cfg->DmaUnit,uart_cfg->DmaCh,uart_cfg->DmaTrigsrc);
    bsp_dma_SetSrcAddr(uart_cfg->DmaUnit,uart_cfg->DmaCh,(uint32_t)(&USARTx->DR)+2);
    bsp_dma_SetDesAddr(uart_cfg->DmaUnit,uart_cfg->DmaCh,(uint32_t)UART1_RXbuff);
    bsp_dma_ch_enable(uart_cfg->DmaUnit,uart_cfg->DmaCh,Enable);
    bsp_interrupt_callback_regist(INT_DMA1_TC0,USART1_RXDMA_IRQn,uart_cfg->rx_dma_callback);
#else
    bsp_interrupt_callback_regist(INT_USART1_RI,USART1_RX_IRQn,uart_cfg->uart_rx_callback);
#endif
}
static void bsp_uart4_cfg(M4_USART_TypeDef *USARTx,bsp_uart_cfg_t *uart_cfg)
{
    
    bsp_interrupt_callback_regist(INT_USART4_EI,USART4_ER_IRQn,uart_cfg->uart_err_callback);
#ifdef UART_RXDMA_EN
    bsp_dma_init(uart_cfg->DmaUnit,uart_cfg->DmaCh,DMA_SRC_ADDR_FIX,DMA_DEST_ADDR_INC,DMA_DATAWIDTH_8BIT);
    bsp_dma_set_count(uart_cfg->DmaUnit,uart_cfg->DmaCh,RXLEN);
    bsp_dma_set_TrigSrc(uart_cfg->DmaUnit,uart_cfg->DmaCh,uart_cfg->DmaTrigsrc);
    bsp_dma_SetSrcAddr(uart_cfg->DmaUnit,uart_cfg->DmaCh,(uint32_t)(&USARTx->DR)+2);
    bsp_dma_SetDesAddr(uart_cfg->DmaUnit,uart_cfg->DmaCh,(uint32_t)UART4_RXbuff);
    bsp_dma_ch_enable(uart_cfg->DmaUnit,uart_cfg->DmaCh,Enable);
    bsp_interrupt_callback_regist(INT_DMA1_TC1,USART4_RXDMA_IRQn,uart_cfg->rx_dma_callback);
#endif
}
uint8_t bsp_uart_init(M4_USART_TypeDef *USARTx,bsp_uart_cfg_t *uart_cfg)
{    
    PWC_Unlock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);
    bsp_usart_gpio_init(uart_cfg);   
    /* Enable peripheral clock */
    if(USARTx == M4_USART1)
    {
        PWC_Fcg3PeriphClockCmd(PWC_FCG3_USART1, Enable); 
        bsp_uart1_cfg(USARTx,uart_cfg);        
    }
    if(USARTx == M4_USART2)
    {
        PWC_Fcg3PeriphClockCmd(PWC_FCG3_USART2, Enable);
    }
    if(USARTx == M4_USART3)
    {
        PWC_Fcg3PeriphClockCmd(PWC_FCG3_USART3, Enable);
    }
    if(USARTx == M4_USART4)
    {
        PWC_Fcg3PeriphClockCmd(PWC_FCG3_USART4, Enable);
        bsp_uart4_cfg(USARTx,uart_cfg);
    }
    if(USARTx == M4_USART5)
    {
        PWC_Fcg3PeriphClockCmd(PWC_FCG3_USART5, Enable);
    }
    if(USARTx == M4_USART6)
    {
        PWC_Fcg3PeriphClockCmd(PWC_FCG3_USART6, Enable);
    }
    if(USARTx == M4_USART7)
    {
        PWC_Fcg3PeriphClockCmd(PWC_FCG3_USART7, Enable);
    }
    if(USARTx == M4_USART8)
    {
        PWC_Fcg3PeriphClockCmd(PWC_FCG3_USART8, Enable);
    }
    if(USARTx == M4_USART9)
    {
        PWC_Fcg3PeriphClockCmd(PWC_FCG3_USART9, Enable);
    }
    if(USARTx == M4_USART10)
    {
        PWC_Fcg3PeriphClockCmd(PWC_FCG3_USART10, Enable);
    }    
    PWC_Lock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);
    USART_UartInit(USARTx, &(uart_cfg->stcUartConf));
    USART_FuncCmd(USARTx, USART_TX, Enable);
	USART_FuncCmd(USARTx, USART_RX, Enable);    
    USART_FuncCmd(USARTx, USART_INT_RX, Enable);
    if(uart_cfg->RTO_enable) 
    {
        USART_FuncCmd(USARTx, USART_RTO, Enable);
        USART_FuncCmd(USARTx, USART_INT_RTO, Enable);
    } 
    return Ok;
}
inline void bsp_usart_transfer_int(M4_USART_TypeDef *USARTx,uint8_t *data, uint32_t len)
{
   
    
}
inline uint8_t bsp_usart_transfer_wait(M4_USART_TypeDef *USARTx,uint8_t *data, uint32_t len)
{
    uint32_t tick;
    tick = SysTick_GetTick();
    for(int i=0;i<len;i++)
    {
        while(USART_GetStatus(USARTx,USART_FLAG_TXE) == 0)
        {
            if(SysTick_GetTick()-tick > 10)
            {
                return ErrorTimeout;
            }
        }
        USART_SendData(USARTx,data[i]);
    }
    tick = SysTick_GetTick();
    while(USART_GetStatus(USARTx,USART_FLAG_TC) == 0)
    {
        if(SysTick_GetTick()-tick > 10)
        {
            return ErrorTimeout;
        }
    }
    return Ok;
}

void bsp_uart1_init(void)
{
    bsp_uart_cfg_t uartcfg;
    uartcfg.Baudrate = 115200;
    uartcfg.RTO_enable = Enable;
    uartcfg.rxPort = UART1_RX_PORT;
    uartcfg.rxPin = UART1_RX_PIN;
    uartcfg.rxfunc = UART1_RX_FUNC;
    uartcfg.txPort = UART1_TX_PORT;
    uartcfg.txPin = UART1_TX_PIN;
    uartcfg.txfunc = UART1_TX_FUNC;
    uartcfg.uart_err_callback = usart1_error_callback;
    uartcfg.uart_timeout_callback = usart1_rx_timerout_callback;
#ifdef UART_RXDMA_EN   
    uartcfg.DmaUnit = UART1_RX_DMA;
    uartcfg.DmaCh = UART1_RX_CH;
    uartcfg.DmaTrigsrc = UART1_DMA_TRIG;
    uartcfg.rx_dma_callback = usart1_dma_callback;
#else
    uartcfg.uart_rx_callback = usart1_rx_callback;
#endif
    uartcfg.stcUartConf.u32Baudrate = 115200;
    uartcfg.stcUartConf.u32BitDirection = USART_LSB;//低位在前
    uartcfg.stcUartConf.u32ClkMode = USART_INTERNCLK_OUTPUT;//不输出时钟
    uartcfg.stcUartConf.u32DataWidth = USART_DATA_LENGTH_8BIT;//8位数据长度
    uartcfg.stcUartConf.u32HwFlowCtrl = USART_HWFLOWCTRL_NONE;//无流控
    uartcfg.stcUartConf.u32NoiseFilterState = USART_NOISE_FILTER_DISABLE;//无噪声滤波
    uartcfg.stcUartConf.u32OversamplingBits = USART_OVERSAMPLING_8BIT;//8个时钟采样
    uartcfg.stcUartConf.u32Parity = USART_PARITY_NONE;//无奇偶校验
    uartcfg.stcUartConf.u32PclkDiv = USART_PCLK_DIV16;//16分频
    uartcfg.stcUartConf.u32SbDetectPolarity = USART_SB_DETECT_FALLING;//下降沿起始
    uartcfg.stcUartConf.u32StopBit = USART_STOPBIT_1BIT;//1个停止位
    bsp_uart_init(M4_USART1,&uartcfg);
}
static void usart4_dma_callback(void)
{
    bsp_dma_SetDesAddr(UART4_RX_DMA,UART4_RX_CH,(uint32_t)UART4_RXbuff);
    bsp_dma_set_count(UART4_RX_DMA,UART4_RX_CH,RXLEN);
    bsp_dma_ch_enable(UART4_RX_DMA,UART4_RX_CH,Enable);   
    bsp_usart_transfer_wait(M4_USART4,UART4_RXbuff,RXLEN);
}
static void usart4_error_callback(void)
{
    ;
}
static void usart4_rx_callback(void)
{
    ;
}
static void usart4_tc_callback(void)
{
    USART_FuncCmd(M4_USART4, USART_INT_TC, Disable);
    LOG_INFO("usart4_tc_callback");
}
void bsp_uart4_init(void)
{
    bsp_uart_cfg_t uartcfg;
    
    GPIO_Unlock();
	GPIO_SetDebugPort(GPIO_PIN_DEBUG_JTAG,Disable);
	GPIO_Lock();
    
    uartcfg.Baudrate = 115200;
    uartcfg.RTO_enable = Disable;
    uartcfg.rxPort = UART4_RX_PORT;
    uartcfg.rxPin = UART4_RX_PIN;
    uartcfg.rxfunc = UART4_RX_FUNC;
    uartcfg.txPort = UART4_TX_PORT;
    uartcfg.txPin = UART4_TX_PIN;
    uartcfg.txfunc = UART4_TX_FUNC;
    uartcfg.uart_err_callback = usart4_error_callback;
    uartcfg.uart_timeout_callback = NULL;
    
#ifdef UART_RXDMA_EN   
    uartcfg.DmaUnit = UART4_RX_DMA;
    uartcfg.DmaCh = UART4_RX_CH;
    uartcfg.DmaTrigsrc = UART4_DMA_TRIG;
    uartcfg.rx_dma_callback = usart4_dma_callback;
#endif
    uartcfg.stcUartConf.u32Baudrate = 115200;
    uartcfg.stcUartConf.u32BitDirection = USART_LSB;//低位在前
    uartcfg.stcUartConf.u32ClkMode = USART_INTERNCLK_OUTPUT;//不输出时钟
    uartcfg.stcUartConf.u32DataWidth = USART_DATA_LENGTH_8BIT;//8位数据长度
    uartcfg.stcUartConf.u32HwFlowCtrl = USART_HWFLOWCTRL_NONE;//无流控
    uartcfg.stcUartConf.u32NoiseFilterState = USART_NOISE_FILTER_DISABLE;//无噪声滤波
    uartcfg.stcUartConf.u32OversamplingBits = USART_OVERSAMPLING_8BIT;//8个时钟采样
    uartcfg.stcUartConf.u32Parity = USART_PARITY_NONE;//无奇偶校验
    uartcfg.stcUartConf.u32PclkDiv = USART_PCLK_DIV16;//16分频
    uartcfg.stcUartConf.u32SbDetectPolarity = USART_SB_DETECT_FALLING;//下降沿起始
    uartcfg.stcUartConf.u32StopBit = USART_STOPBIT_1BIT;//1个停止位
    bsp_uart_init(M4_USART4,&uartcfg);
    USART_FuncCmd(M4_USART4,USART_INT_TC,Enable);
    bsp_interrupt_callback_regist(INT_USART4_TCI, Int092_IRQn, usart4_tc_callback);
}
