/**
 *******************************************************************************
 * @file  usb/usb_dev_hid_custom/source/usb_dev_user.c
 * @brief User callback function for USB example
 @verbatim
   Change Logs:
   Date             Author          Notes
   2020-06-12       CDT             First version
 @endverbatim
 *******************************************************************************
 * Copyright (C) 2020, Huada Semiconductor Co., Ltd. All rights reserved.
 *
 * This software component is licensed by HDSC under BSD 3-Clause license
 * (the "License"); You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                    opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

/*******************************************************************************
 * Include files
 ******************************************************************************/
#include "hc32_ddl.h"
#include "usb_dev_user.h"
#include "usb_dev_ctrleptrans.h"
#include "usb_app_conf.h"
#include "usb_bsp.h"


void usb_dev_user_init(void);
void usb_dev_user_rst (void);
void usb_dev_user_devcfg (void);
void usb_dev_user_devsusp(void);
void usb_dev_user_devresume(void);
void usb_dev_user_conn(void);
void usb_dev_user_disconn(void); 
/*******************************************************************************
 * Local type definitions ('typedef')
 ******************************************************************************/
usb_dev_user_func user_cb =
{
    &usb_dev_user_init,
    &usb_dev_user_rst,
    &usb_dev_user_devcfg,
    &usb_dev_user_devsusp,
    &usb_dev_user_devresume,
    &usb_dev_user_conn,
    &usb_dev_user_disconn,
};

/*******************************************************************************
 * Local pre-processor symbols/macros ('#define')
 ******************************************************************************/

/*******************************************************************************
 * Global variable definitions (declared in header file with 'extern')
 ******************************************************************************/

/*******************************************************************************
 * Local function prototypes ('static')
 ******************************************************************************/

/*******************************************************************************
 * Local variable definitions ('static')
 ******************************************************************************/

/*******************************************************************************
 * Function implementation - global ('extern') and local ('static')
 ******************************************************************************/
/**
 *******************************************************************************
 ** \brief  usb_dev_user_init
 ** \param  none
 ** \retval none
 ******************************************************************************/
void usb_dev_user_init(void)
{

}

/**
 *******************************************************************************
 ** \brief  usb_dev_user_rst
 ** \param  none
 ** \retval none
 ******************************************************************************/
void usb_dev_user_rst(void)
{
#if (DDL_PRINT_ENABLE == DDL_ON)
    DDL_Printf(">>USB Device has reseted.\r\n" );
    DDL_Printf(">>The interval report is 100ms\r\n");
#endif
}

/**
 *******************************************************************************
 ** \brief  usb_dev_user_devcfg
 ** \param  none
 ** \retval none
 ******************************************************************************/
void usb_dev_user_devcfg (void)
{
#if (DDL_PRINT_ENABLE == DDL_ON)
    DDL_Printf(">>HID interface starts.\r\n");
#endif
}

/**
 *******************************************************************************
 ** \brief  usb_dev_user_conn
 ** \param  none
 ** \retval none
 ******************************************************************************/
void usb_dev_user_conn (void)
{
#if (DDL_PRINT_ENABLE == DDL_ON)
    DDL_Printf(">>USB device connects.\r\n");
#endif
}

/**
 *******************************************************************************
 ** \brief  USBD_USR_DeviceDisonnected
 ** \param  none
 ** \retval none
 ******************************************************************************/
void usb_dev_user_disconn (void)
{
#if (DDL_PRINT_ENABLE == DDL_ON)
    DDL_Printf(">>USB device disconnected.\r\n");
#endif
}

/**
 *******************************************************************************
 ** \brief  usb_dev_user_devsusp
 ** \param  none
 ** \retval none
 ******************************************************************************/
void usb_dev_user_devsusp(void)
{
#if (DDL_PRINT_ENABLE == DDL_ON)
    DDL_Printf(">>USB device in suspend status.\r\n");
#endif
}

/**
 *******************************************************************************
 ** \brief  usb_dev_user_devresume
 ** \param  none
 ** \retval none
 ******************************************************************************/
void usb_dev_user_devresume(void)
{
#if (DDL_PRINT_ENABLE == DDL_ON)
    DDL_Printf(">>USB device resumes.\r\n");
#endif
}

/*******************************************************************************
 * EOF 
 ******************************************************************************/
