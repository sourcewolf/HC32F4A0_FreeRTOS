/**
 *******************************************************************************
 * @file  usb/usb_dev_hid_custom/source/usb_dev_desc.h
 * @brief Head file for usb_dev_desc.c
 @verbatim
   Change Logs:
   Date             Author          Notes
   2020-06-12       CDT             First version
 @endverbatim
 *******************************************************************************
 * Copyright (C) 2020, Huada Semiconductor Co., Ltd. All rights reserved.
 *
 * This software component is licensed by HDSC under BSD 3-Clause license
 * (the "License"); You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                    opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */
#ifndef __USB_DEV_DESC_H__
#define __USB_DEV_DESC_H__

/*******************************************************************************
 * Include files
 ******************************************************************************/
#include "usb_dev_def.h"
#include "stdint.h"
/*******************************************************************************
 * Global pre-processor symbols/macros ('#define')
 ******************************************************************************/
#define USB_DEVICE_DESCRIPTOR_TYPE              0x01
#define USB_CFG_DESCRIPTOR_TYPE                 0x02
#define USB_STRING_DESCRIPTOR_TYPE              0x03
#define USB_INTERFACE_DESCRIPTOR_TYPE           0x04
#define USB_ENDPOINT_DESCRIPTOR_TYPE            0x05
#define USB_SIZ_DEVICE_DESC                     18
#define USB_SIZ_STRING_LANGID                   4

/*******************************************************************************
 * Global variable definitions ('extern')
 ******************************************************************************/
extern  uint8_t usb_dev_devicedesc[USB_SIZ_DEVICE_DESC];
extern  uint8_t usb_dev_strdesc[USB_MAX_STR_DESC_SIZ];
extern  uint8_t USB_DEV_LangIDDesc[USB_SIZ_STRING_LANGID];
extern  usb_dev_desc_func USR_desc;

/*******************************************************************************
  Global function prototypes (definition in C source)
 ******************************************************************************/

#endif /* __USBD_DESC_H__ */

