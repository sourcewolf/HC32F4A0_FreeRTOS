/**
 *******************************************************************************
 * @file  usb/usb_dev_hid_custom/source/usb_dev_user.h
 * @brief Head file for usb_dev_user.c
 @verbatim
   Change Logs:
   Date             Author          Notes
   2020-06-12       CDT             First version
 @endverbatim
 *******************************************************************************
 * Copyright (C) 2020, Huada Semiconductor Co., Ltd. All rights reserved.
 *
 * This software component is licensed by HDSC under BSD 3-Clause license
 * (the "License"); You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                    opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */
#ifndef __USB_DEV_USER_H__
#define __USB_DEV_USER_H__

/*******************************************************************************
 * Include files
 ******************************************************************************/
#include "usb_dev_core.h"

/*******************************************************************************
 * Global type definitions ('typedef')
 ******************************************************************************/
extern  usb_dev_user_func user_cb;

/*******************************************************************************
  Global function prototypes (definition in C source)
 ******************************************************************************/

#endif /*__USB_DEV_USER_H__*/

/*******************************************************************************
 * EOF (not truncated)
 ******************************************************************************/
