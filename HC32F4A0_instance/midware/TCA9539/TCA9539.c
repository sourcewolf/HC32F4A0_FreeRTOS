#include "TCA9539.h"
void TCA9539_init(void)
{
	bsp_i2c_init(I2C1_UNIT,200000);
    TCA9539_CFG(0,0);//全部设置成输出
}
void TCA9539_cmd(uint8_t cmd, uint8_t data)
{
	I2C_Write_data(I2C1_UNIT,0x74,&cmd,1,&data,1);
}
void TCA9539_CFG(uint8_t cfg0,uint8_t cfg1)
{
	TCA9539_cmd(CMDCFG0,cfg0);
	DDL_DelayMS(1);
	TCA9539_cmd(CMDCFG1,cfg1);
}
void ledcontrol(uint8_t ledstatus)
{
	TCA9539_SetPORT(EXPort1,ledstatus&0xE0,ValueH);
    TCA9539_SetPORT(EXPort1,(~ledstatus)&0xE0,ValueL);
}
uint8_t TCA9539_ReadPort(uint8_t port)
{
    uint8_t value;
    uint8_t command;
    command = port+CMDINPUT0;
    I2C_Read_data(I2C1_UNIT,0x74,&command,1,&value,1);
    return value;
}
void TCA9539_SetPORT(uint8_t port, uint8_t Pin_data,bool value)
{
    uint8_t pin_t;
    pin_t = TCA9539_ReadPort(port);
    DDL_DelayMS(1);
    if(value == ValueH)
    {
        pin_t |= Pin_data;
    }
    else
    {
        pin_t &= ~Pin_data;
    }
    TCA9539_cmd(port+CMDOUTPUT0,pin_t);
    DDL_DelayMS(1);
}