/**
 *******************************************************************************
 * @file  usb_dev_hid_cdc_wrapper.h
 * @brief Head file for usb_dev_hid_cdc_wrapper.c
 @verbatim
   Change Logs:
   Date             Author          Notes
   2021-05-17       linsq           First version
 @endverbatim
 *******************************************************************************
 * Copyright (C) 2020, Huada Semiconductor Co., Ltd. All rights reserved.
 *
 * This software component is licensed by HDSC under BSD 3-Clause license
 * (the "License"); You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                    opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */
#ifndef __USB_DEV_HID_CDC_WRAPPER_H_
#define __USB_DEV_HID_CDC_WRAPPER_H_

/*******************************************************************************
 * Include files
 ******************************************************************************/
#include  "usb_dev_ctrleptrans.h"

/*******************************************************************************
 * Global variable definitions ('extern')
 ******************************************************************************/
extern usb_dev_class_func  class_composite_cbk;

#endif /* __USB_DEV_HID_CDC_WRAPPER_H_ */

/*******************************************************************************
 * EOF (not truncated)
 ******************************************************************************/
