/**
 *******************************************************************************
 * @file  usb_dev_hid_msc_wrapper.h
 * @brief header file for the usb_dev_hid_msc_wrapper.c
 @verbatim
   Change Logs:
   Date             Author          Notes
   2019-11-19       CDT             First version
 @endverbatim
 *******************************************************************************
 * Copyright (C) 2020, Huada Semiconductor Co., Ltd. All rights reserved.
 *
 * This software component is licensed by HDSC under BSD 3-Clause license
 * (the "License"); You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                    opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */
#ifndef __USB_DEV_HID_MSC_WRAPPER_H__
#define __USB_DEV_HID_MSC_WRAPPER_H__

/*******************************************************************************
 * Include files
 ******************************************************************************/
#include  "usb_dev_ctrleptrans.h"

/*******************************************************************************
 * Global pre-processor symbols/macros ('#define')
 ******************************************************************************/

#define USB_MSC_HID_CONFIG_DESC_SIZ  (USB_CUSTOM_HID_CONFIG_DESC_SIZ -9u + USB_MSC_CONFIG_DESC_SIZ)

/*******************************************************************************
 * Global variable definitions ('extern')
 ******************************************************************************/
extern usb_dev_class_func  usb_dev_composite_cbk;

#endif  /* __USB_DEV_HID_MSC_WRAPPER_H__ */

/*******************************************************************************
 * EOF (not truncated)
 ******************************************************************************/
