/**
 *******************************************************************************
 * @file  usb_dev_audio_in_itf.h
 * @brief Head file for usb_dev_audio_in_itf.c
 @verbatim
   Change Logs:
   Date             Author          Notes
   2021-03-29       Linsq           First version
 @endverbatim
 *******************************************************************************
 * Copyright (C) 2020, Huada Semiconductor Co., Ltd. All rights reserved.
 *
 * This software component is licensed by HDSC under BSD 3-Clause license
 * (the "License"); You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                    opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */
#ifndef __USB_DEV_AUDIO_IN_ITF_H__
#define __USB_DEV_AUDIO_IN_ITF_H__

#include "codec.h"

typedef enum
{
  AUDIO_IN_CMD_START = 1,
  AUDIO_IN_CMD_STOP,
}AUDIO_IN_CMD_TypeDef;

/* Mute commands */
#define AUDIO_IN_MUTE                   0x01
#define AUDIO_IN_UNMUTE                 0x00

/* Functions return value */
#define AUDIO_OK                        0x00
#define AUDIO_FAIL                      0xFF

/* Audio Machine States */
#define AUDIO_IN_STATE_INACTIVE         0x00
#define AUDIO_IN_STATE_ACTIVE           0x01
#define AUDIO_IN_STATE_RECORDING        0x02
#define AUDIO_IN_STATE_STOPPED          0x03
#define AUDIO_IN_STATE_ERROR            0x04

#define DEFAULT_IN_AUDIO_FREQ           48000
#define MAX_AUDIO_IN_FREQ               DEFAULT_IN_AUDIO_FREQ
#define MIN_AUDIO_IN_FREQ               8000

extern AUDIO_FOPS_TypeDef  AUDIO_IN_fops;


#endif //__USB_DEV_AUDIO_IN_ITF_H__



