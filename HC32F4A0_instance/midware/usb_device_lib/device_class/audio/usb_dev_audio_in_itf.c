/**
 *******************************************************************************
 * @file  usb_dev_audio_in_itf.c
 * @brief Audio class for USB device
 @verbatim
   Change Logs:
   Date             Author          Notes
   2021-03-29       Linsq           First version
 @endverbatim
 *******************************************************************************
 * Copyright (C) 2020, Huada Semiconductor Co., Ltd. All rights reserved.
 *
 * This software component is licensed by HDSC under BSD 3-Clause license
 * (the "License"); You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                    opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */
 
/*******************************************************************************
 * Include files
 ******************************************************************************/
#include "usb_dev_audio_class.h"
#include "usb_dev_audio_in_itf.h"

void     record_init         (void);
void     record_deinit       (void);
uint8_t  record_audiocmd     (uint8_t* pbuf, uint32_t size, uint8_t cmd);
uint8_t  record_volumectrl   (uint8_t vol);
uint8_t  record_mutectrl      (uint8_t cmd);
uint8_t  record_optionctrl   (uint8_t cmd);
uint8_t  record_getstate     (void);




AUDIO_FOPS_TypeDef  AUDIO_IN_fops = 
{
    record_init,
    record_deinit,
    record_audiocmd,
    record_volumectrl,
    record_mutectrl,
    record_optionctrl,
    record_getstate
};

uint8_t AudioInState = AUDIO_IN_STATE_INACTIVE;   


/**
 *******************************************************************************
 ** \brief  initialize resources requered by the audio recorder function
 ** \retval none
 ******************************************************************************/
void record_init(void)
{
  static uint32_t Initialized = 0;
  
  /* Check if the low layer has already been initialized */
  if (Initialized == 0)
  {
    /* Call low layer function */
    
    /* Set the Initialization flag to prevent reinitializing the interface again */
    Initialized = 1;
  }
  
  /* Update the Audio state machine */
  AudioInState = AUDIO_IN_STATE_ACTIVE;
}

/**
 *******************************************************************************
 ** \brief  deinit all resources required by audio recorder function
 ** \param  none
 ** \retval none
 ******************************************************************************/
void record_deinit(void)
{   
  /* Update the Audio state machine */
  AudioInState = AUDIO_IN_STATE_INACTIVE;
}

/**
 *******************************************************************************
 ** \brief  control the current stream,such as record or stop etc.
 ** \param  pbuf: address to which file shoud be stored
 ** \param  size: size of the current buffer in bytes
 ** \param  cmd: the command to be executed
 ** \retval status
 ******************************************************************************/
uint8_t record_audiocmd(uint8_t* pbuf, uint32_t size, uint8_t cmd)
{
  /* Check the current state */
  if ((AudioInState == AUDIO_IN_STATE_INACTIVE) || (AudioInState == AUDIO_IN_STATE_ERROR))
  {
    AudioInState = AUDIO_IN_STATE_ERROR;
    return AUDIO_FAIL;
  }
  
  switch (cmd)
  {
    /* Process the RECORD command ----------------------------*/
  case AUDIO_IN_CMD_START:
    /* If current state is Active or Stopped */
    if ((AudioInState == AUDIO_IN_STATE_ACTIVE) || \
       (AudioInState == AUDIO_IN_STATE_STOPPED))
    {
      /* Start recording */
        Record_Start((uint32_t)pbuf , size);  //Start to record
        PORT_SetBits(LED0_PORT, LED0_PIN);    //lighten the Red Led
      /* Update state machine */
      AudioInState = AUDIO_IN_STATE_RECORDING;
      return AUDIO_OK;
    } 
    else /* Not allowed command */
    {
      return AUDIO_FAIL;
    }
    
    /* Process the STOP command ----------------------------*/
  case AUDIO_IN_CMD_STOP:
    if (AudioInState != AUDIO_IN_STATE_RECORDING)
    {
      /* Unsupported command */
      return AUDIO_FAIL;
    }
    else
    {
      /* Call low layer function */
        Record_Stop();                       //Stop recording
        PORT_ResetBits(LED0_PORT, LED0_PIN); //Extinguish the Red Led
      /* Update state */
      AudioInState = AUDIO_IN_STATE_STOPPED;
      return AUDIO_OK;
    }
  
    /* Unsupported command ---------------------------------*/
  default:
    return AUDIO_FAIL;
  }  
}

/**
 *******************************************************************************
 ** \brief  set the volume level in percentage
 ** \param  vol: between 0 and 100 
 ** \retval status
 ******************************************************************************/
uint8_t record_volumectrl(uint8_t vol)
{
    //TODO:
    return AUDIO_OK;
}

/**
 *******************************************************************************
 ** \brief  the command to mute or unmute the audio recorder
 ** \param  cmd: command
 ** \retval status
 ******************************************************************************/
uint8_t record_mutectrl(uint8_t cmd)
{
    //TODO
    return AUDIO_OK;
}

/**
 *******************************************************************************
 ** \brief  option control function
 ** \param  cmd: command
 ** \retval status
 ******************************************************************************/
uint8_t record_optionctrl(uint8_t cmd)
{  
    //TODO
    return AUDIO_OK;
}

/**
 *******************************************************************************
 ** \brief  get the current state of the audio recorder
 ** \param  none
 ** \retval status
 ******************************************************************************/
uint8_t record_getstate(void)
{
    //TODO
    return AudioInState;
}
