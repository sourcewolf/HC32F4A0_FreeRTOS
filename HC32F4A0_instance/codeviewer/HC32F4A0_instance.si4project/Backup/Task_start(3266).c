#include "Task_start.h"
#include "bsp_timer6_freq_meter.h"
TaskHandle_t Hd_Task_Start;
void Task_START(void *param)
{
	bsp_timer6_counter_init();
	bsp_timer6_timer_1ms_init();
    while(1)
    {
    }
}
void Start_Task_Create(void)
{
	xTaskCreate(Task_START,(const char *)"LED", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+3, &Hd_Task_Start );		    
    vTaskStartScheduler();
}
