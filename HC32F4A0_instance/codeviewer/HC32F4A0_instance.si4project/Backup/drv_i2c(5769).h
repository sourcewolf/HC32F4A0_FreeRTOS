#include "hc32_ddl.h"
#include "drv_gpio.h"
#include "bsp_i2c.h"
typedef struct hc32_i2c_obj
{
	M4_I2C_TypeDef *pstcI2Cx;
	uint32_t baudrate;
	uint8_t scl_pin;
    uint8_t scl_pin_func;
	uint8_t sda_pin;
    uint8_t sda_pin_func;
	uint32_t timerout;
}hc32_i2c_obj_t;