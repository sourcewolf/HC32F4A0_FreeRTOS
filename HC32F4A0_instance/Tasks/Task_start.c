#include "Task_start.h"

TaskHandle_t Hd_Task_Start;
uint8_t test_txdata[10]= {0,1,2,3,4,5,6,7,8,9}, test_rxdata[10];
void Task_START(void *param)
{
    while(1)
    {
		bsp_led_toggle();
		ledcontrol(0xFF);//
		vTaskDelay(500/portTICK_PERIOD_MS);
		ledcontrol(0x00);
		vTaskDelay(500/portTICK_PERIOD_MS);
    }
}
void OS_Start(void)
{
	xTaskCreate(Task_START,(const char *)"LED", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+3, &Hd_Task_Start );		    
    vTaskStartScheduler();
}
