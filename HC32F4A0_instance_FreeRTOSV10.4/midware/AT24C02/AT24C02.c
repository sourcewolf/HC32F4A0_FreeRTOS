#include "bsp_i2c.h"
#ifdef RTT_DEBUG
#include "SEGGER_RTT.h"
#endif
uint8_t txdata[100]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28},rxdata[100];
uint8_t EEPROM_Write(uint32_t addr, uint8_t *data,uint8_t len)
{
	uint32_t pos = 0;
	uint8_t trytime = 0;
	uint8_t status;
	if((addr & 0x07) != 0)
	{
		do
		{
			trytime++;
			if(trytime>3)
			{
				trytime = 0;
				break;
			}
			if(len>(8-(addr & 0x07)))
			{
				status = I2C_Write_data(I2C1_UNIT,0x50,(uint8_t *)&addr, 1, data+pos,(addr & 0x07));//
				len -= (8-(addr & 0x07));
				pos += (8-(addr & 0x07));
				addr += (8-(addr & 0x07));
			}
			else
			{
				status = I2C_RET_OK;
				break;//直接跳出本循环
			}
			DDL_DelayMS(10);
		}while(status !=I2C_RET_OK);
	}
	while(len>8)
	{
		do
		{
			trytime++;
			if(trytime>3)
			{
				trytime = 0;
				break;
			}
			status = I2C_Write_data(I2C1_UNIT,0x50,(uint8_t *)&addr,1,data+pos,8);		
			DDL_DelayMS(10);
		}while(status !=I2C_RET_OK);
		addr+=8;
		len -= 8;
		pos += 8;
	}
	do
	{
		trytime++;
		if(trytime>3)
		{
			trytime = 0;
			break;
		}
		status = I2C_Write_data(I2C1_UNIT,0x50,(uint8_t *)&addr,1,data+pos,len);		
		DDL_DelayMS(10);
	}while(status !=I2C_RET_OK);
	return status;
}
uint8_t EEPROM_Read(uint32_t addr, uint8_t *data,uint8_t len)
{
	uint8_t trytime = 0;
	uint8_t status;	
	trytime = 0;
	do
	{
		trytime++;
		if(trytime>3)
		{
			trytime = 0;
			break;
		}
		status = I2C_Read_data(I2C1_UNIT,0x50,(uint8_t *)&addr,1,data,len);	
		DDL_DelayMS(10);
	}while(status !=I2C_RET_OK);
	return status;
}
void TestEEPROM(void)
{
	uint8_t trytime = 0;
	uint8_t status;
	do
	{
		trytime++;
		if(trytime>3)
		{
			trytime = 0;
			break;
		}
		status = I2C_Write_data(I2C1_UNIT,0x54,NULL,1,txdata,8);	
#ifdef RTT_DEBUG
		SEGGER_RTT_printf(0,"0x54 Write I2C_Status1 = %d,trytime = %d\r\n",status,trytime);	
#endif		
		DDL_DelayMS(10);
	}while(status !=I2C_RET_OK);
	status = EEPROM_Write(0x07,txdata,25);
	status = EEPROM_Read(0x07,rxdata,8);
}
