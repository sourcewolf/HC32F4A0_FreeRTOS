#include "bsp_spi.h"
#define EXAMPLE_WIRE_MODE       (SPI_WIRE_3)
#define EXAMPLE_SPI_MODE        (SPI_MODE_1)
bsp_spi_port_cfg_t port_spi1,port_spi2,port_spi3,port_spi4,port_spi5,port_spi6;
/* function name: bsp_spi4_error_callback
*功能：spi4错误中断
*输入参数：无
*输出参数：无
*/
static void bsp_spi4_error_callback(void)
{
    if(Set == SPI_GetStatus(SPI_UNIT4, SPI_FLAG_OVERLOAD))//接收过载
    {
        SPI_ClearFlag(SPI_UNIT4,SPI_FLAG_OVERLOAD);
    }
    if(Set == SPI_GetStatus(SPI_UNIT4, SPI_FLAG_PARITY_ERROR))//奇偶检验失败
    {
        SPI_ClearFlag(SPI_UNIT4,SPI_FLAG_PARITY_ERROR);
    }
    if(Set == SPI_GetStatus(SPI_UNIT4, SPI_FLAG_UNDERLOAD))//发送欠载
    {
        SPI_ClearFlag(SPI_UNIT4,SPI_FLAG_UNDERLOAD);
    }
    if(Set == SPI_GetStatus(SPI_UNIT4, SPI_FLAG_MODE_FAULT))//模式错误
    {
        SPI_ClearFlag(SPI_UNIT4,SPI_FLAG_MODE_FAULT);
    }
}
/* function name: bsp_spi_gpio_cfg
*功能：spi IO配置
*输入参数：M4_SPI_TypeDef *SPIx  SPI_UNIT1, SPI_UNIT2, SPI_UNIT3, SPI_UNIT4, SPI_UNIT5, SPI_UNIT6
           bsp_spi_cfg_t spi_cfg
*输出参数：uint8_t SPI_RET_OK，SPI_RET_ERROR SPI_BUSY SPI_TIMEROUT SPI_BADPARA
*/
uint8_t bsp_spi_gpio_cfg(M4_SPI_TypeDef *SPIx, bsp_spi_cfg_t *spi_cfg)
{
    (void)SPIx;
    GPIO_Unlock();
    if(spi_cfg->stcSpiInit.u32WireMode == SPI_WIRE_4)
    {
        GPIO_SetFunc(spi_cfg->port->CsPort, spi_cfg->port->CsPin, spi_cfg->port->Csfunc, PIN_SUBFUNC_DISABLE);
    }
    else
    {
        stc_gpio_init_t stcGpioInit;
        GPIO_StructInit(&stcGpioInit);
        stcGpioInit.u16PinDir = PIN_DIR_OUT;
        GPIO_Init(spi_cfg->port->CsPort, spi_cfg->port->CsPin, &stcGpioInit);
        GPIO_SetPins(spi_cfg->port->CsPort, spi_cfg->port->CsPin);
    }
	GPIO_SetFunc(spi_cfg->port->ClkPort, spi_cfg->port->ClkPin, spi_cfg->port->Clkfunc, PIN_SUBFUNC_DISABLE);
    GPIO_SetFunc(spi_cfg->port->MosiPort, spi_cfg->port->MosiPin, spi_cfg->port->Mosifunc, PIN_SUBFUNC_DISABLE);
    GPIO_SetFunc(spi_cfg->port->MisoPort, spi_cfg->port->MisoPin, spi_cfg->port->Misofunc, PIN_SUBFUNC_DISABLE);
	GPIO_Lock();
    return SPI_RET_OK;
}
/* function name: bsp_spi_init
*功能：spi 初始化
*输入参数：M4_SPI_TypeDef *SPIx  SPI_UNIT1, SPI_UNIT2, SPI_UNIT3, SPI_UNIT4, SPI_UNIT5, SPI_UNIT6
           bsp_spi_cfg_t spi_cfg
*输出参数：uint8_t SPI_RET_OK，SPI_RET_ERROR SPI_BUSY SPI_TIMEROUT SPI_BADPARA
*/
uint8_t bsp_spi_init(M4_SPI_TypeDef *SPIx,bsp_spi_cfg_t *spi_cfg)
{
    stc_spi_delay_t spi_delay_cfg;
    SPI_DelayStructInit(&spi_delay_cfg);
    PWC_Unlock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);//打开PWC锁
    /*使能时钟*/
	if(SPIx == SPI_UNIT1)
	{
		PWC_Fcg1PeriphClockCmd(SPI1_CLK,Enable);
	}
	else if(SPIx == SPI_UNIT2)
	{
		PWC_Fcg1PeriphClockCmd(SPI2_CLK,Enable);
	}
	else if(SPIx == SPI_UNIT3)
	{
		PWC_Fcg1PeriphClockCmd(SPI3_CLK,Enable);
	}
	else if(SPIx == SPI_UNIT4)
	{
		PWC_Fcg1PeriphClockCmd(SPI4_CLK,Enable);
	}
	else if(SPIx == SPI_UNIT5)
	{
		PWC_Fcg1PeriphClockCmd(SPI5_CLK,Enable);
	}
	else if(SPIx == SPI_UNIT6)
	{
		PWC_Fcg1PeriphClockCmd(SPI6_CLK,Enable); 
	}
	else
	{
		return SPI_BADPARA;
	}
    /*PWC锁*/
	PWC_Lock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);  
    /*反初始化SPI*/
    SPI_DeInit(SPIx);
    /*初始化SPI*/
    SPI_Init(SPIx, &spi_cfg->stcSpiInit);
    /*使能SPI模块*/
    SPI_FunctionCmd(SPIx,Enable);
    /*设置延时时间*/
    SPI_DelayTimeCfg(SPIx,&spi_delay_cfg);
    /*使能RX，TX，EE中断*/
    SPI_IntCmd(SPIx,SPI_INT_TX_BUFFER_EMPTY|SPI_INT_RX_BUFFER_FULL|SPI_INT_ERROR,Enable);
    bsp_spi_gpio_cfg(SPIx, spi_cfg);
    return SPI_RET_OK;
}
/* function name: bsp_set_spi_cs
*功能：spi CS脚 设置
*输入参数：M4_SPI_TypeDef *SPIx  SPI_UNIT1, SPI_UNIT2, SPI_UNIT3, SPI_UNIT4, SPI_UNIT5, SPI_UNIT6
           bool value Enable 输出低电平有效，Disable 输出高电平无效
*输出参数：uint8_t SPI_RET_OK，SPI_RET_ERROR SPI_BUSY SPI_TIMEROUT SPI_BADPARA
*/
uint8_t bsp_set_spi_cs(M4_SPI_TypeDef *SPIx, bool value)
{
    bsp_spi_port_cfg_t *spi_port;
    if(SPIx == SPI_UNIT1)
	{
        spi_port = &port_spi1;
	}
	else if(SPIx == SPI_UNIT2)
	{
        spi_port = &port_spi2;
	}
	else if(SPIx == SPI_UNIT3)
	{
        spi_port = &port_spi3;
	}
	else if(SPIx == SPI_UNIT4)
	{
        spi_port = &port_spi4;
	}
	else if(SPIx == SPI_UNIT5)
	{
        spi_port = &port_spi5;
	}
	else if(SPIx == SPI_UNIT6)
	{
        spi_port = &port_spi6;
	}
	else
	{
		return SPI_BADPARA;
	}
    if(value)
    {
        GPIO_ResetPins(spi_port->CsPort,spi_port->CsPin);
    }
    else
    {
        GPIO_SetPins(spi_port->CsPort,spi_port->CsPin);
    }
    return SPI_RET_OK;
}
/* function name: bsp_spi_transmit8
*功能：spi 传输8位数据
*输入参数：M4_SPI_TypeDef *SPIx  SPI_UNIT1, SPI_UNIT2, SPI_UNIT3, SPI_UNIT4, SPI_UNIT5, SPI_UNIT6
uint8_t *txdata 待发送的数据指针
uint8_t *rxdata 接收数据指针
uint32_t len 传输数据长度
*输出参数：uint8_t SPI_RET_OK，SPI_RET_ERROR SPI_BUSY SPI_TIMEROUT SPI_BADPARA
*/
uint8_t bsp_spi_transmit8(M4_SPI_TypeDef *SPIx, uint8_t *txdata, uint8_t *rxdata, uint32_t len)
{
    uint32_t Lasttick = SysTick_GetTick();
    if((SPIx->CR1 & 0x0001) != SPI_WIRE_4)
    {
        bsp_set_spi_cs(SPIx, Enable);        
    }
    for(uint32_t i = 0;i<len;i++)
    {
        while(Reset == SPI_GetStatus(SPIx, SPI_FLAG_TX_BUFFER_EMPTY))
        {
            if(SysTick_GetTick()-Lasttick > TIMEROUT)
            {
                return SPI_TIMEROUT;
            }
        }
        SPI_WriteDataReg(SPIx, (uint32_t)txdata[i]);
        Lasttick = SysTick_GetTick();
        while(Reset == SPI_GetStatus(SPIx, SPI_FLAG_RX_BUFFER_FULL))
        {
            if(SysTick_GetTick()-Lasttick > TIMEROUT)
            {
                return SPI_TIMEROUT;
            }
        }
        rxdata[i] = SPI_ReadDataReg(SPIx);
    }
    Lasttick = SysTick_GetTick();
    if((SPIx->CR1 & 0x01) != SPI_WIRE_4)
    {
        while(Set == SPI_GetStatus(SPIx, SPI_FLAG_IDLE))
        {
            if(SysTick_GetTick()-Lasttick > TIMEROUT)
            {
                bsp_set_spi_cs(SPIx, Disable);
                return SPI_TIMEROUT;
            }
        }
        bsp_set_spi_cs(SPIx, Disable);
    }
    return SPI_RET_OK;
}
/* function name: bsp_spi4_dma_tc_callback
*功能：spi DMA传输完成
*输入参数：无
*输出参数：无
*/
void bsp_spi4_dma_tc_callback(void)
{
    uint32_t Lasttick = SysTick_GetTick();
    Lasttick = SysTick_GetTick();
    if((M4_SPI4->CR1 & 0x01) != SPI_WIRE_4)
    {
        while(Set == SPI_GetStatus(M4_SPI4, SPI_FLAG_IDLE))
        {
            if(SysTick_GetTick()-Lasttick > TIMEROUT)
            {
                bsp_set_spi_cs(M4_SPI4, Disable);
                break;
            }
        }
        bsp_set_spi_cs(M4_SPI4, Disable);
    }
}
/* function name: bsp_spi_transmit8_Dma
*功能：spi DMA传输8位数据
*输入参数：M4_SPI_TypeDef *SPIx  SPI_UNIT1, SPI_UNIT2, SPI_UNIT3, SPI_UNIT4, SPI_UNIT5, SPI_UNIT6
uint8_t *txdata 待发送的数据指针
uint8_t *rxdata 接收数据指针
uint32_t len 传输数据长度
*输出参数：uint8_t SPI_RET_OK，SPI_RET_ERROR SPI_BUSY SPI_TIMEROUT SPI_BADPARA
*/
uint8_t bsp_spi_transmit8_Dma(M4_SPI_TypeDef *SPIx, uint8_t *txdata, uint8_t *rxdata, uint32_t len)
{
    
    if((SPIx->CR1 & 0x0001) != SPI_WIRE_4)
    {
        bsp_set_spi_cs(SPIx, Enable);        
    }
    
    bsp_dma_SetSrcAddr(SPI_DMA_UNIT,SPI_DMA_TX_CH,(uint32_t)txdata);
    bsp_dma_set_count(SPI_DMA_UNIT,SPI_DMA_TX_CH,len);
    bsp_dma_ch_enable(SPI_DMA_UNIT,SPI_DMA_TX_CH,Enable);
        
    bsp_dma_SetDesAddr(SPI_DMA_UNIT,SPI_DMA_RX_CH,(uint32_t)rxdata);
    bsp_dma_set_count(SPI_DMA_UNIT,SPI_DMA_RX_CH,len);
    bsp_dma_ch_enable(SPI_DMA_UNIT,SPI_DMA_RX_CH,Enable);
    
    SPI_FunctionCmd(SPIx,Disable);
    SPI_FunctionCmd(SPIx,Enable);
    return SPI_RET_OK;
}
/* function name: bsp_spi4_init
*功能：sp4 初始化
*输入参数：无
*输出参数：uint8_t SPI_RET_OK，SPI_RET_ERROR SPI_BUSY SPI_TIMEROUT SPI_BADPARA
*/
uint8_t bsp_spi4_init(void)
{
    bsp_spi_cfg_t spi_cfg;
    /*初始化参数设置*/
    spi_cfg.stcSpiInit.u32WireMode          = EXAMPLE_WIRE_MODE;
    spi_cfg.stcSpiInit.u32TransMode         = SPI_FULL_DUPLEX;
    spi_cfg.stcSpiInit.u32MasterSlave       = SPI_MASTER;
    spi_cfg.stcSpiInit.u32SuspMode          = SPI_COM_SUSP_FUNC_OFF;
    spi_cfg.stcSpiInit.u32Modfe             = SPI_MODFE_DISABLE;
    spi_cfg.stcSpiInit.u32Parity            = SPI_PARITY_INVALID;
    spi_cfg.stcSpiInit.u32SpiMode           = EXAMPLE_SPI_MODE;
    spi_cfg.stcSpiInit.u32BaudRatePrescaler = SPI_BR_PCLK1_DIV64;
    spi_cfg.stcSpiInit.u32DataBits          = SPI_DATA_SIZE_8BIT;
    spi_cfg.stcSpiInit.u32FirstBit          = SPI_FIRST_MSB;
    spi_cfg.stcSpiInit.u32FrameLevel        = SPI_FRAME_1;
    spi_cfg.port = &port_spi4;
    port_spi4.CsPort = GPIO_PORT_E;
    port_spi4.CsPin = GPIO_PIN_04;
    port_spi4.Csfunc = GPIO_FUNC_19_SPI4_NSS0;
    port_spi4.ClkPort = GPIO_PORT_E;
    port_spi4.ClkPin = GPIO_PIN_03;
    port_spi4.Clkfunc = GPIO_FUNC_40_SPI4_SCK;
    port_spi4.MosiPort = GPIO_PORT_E;
    port_spi4.MosiPin = GPIO_PIN_05;
    port_spi4.Mosifunc = GPIO_FUNC_41_SPI4_MOSI;
    port_spi4.MisoPort = GPIO_PORT_E;
    port_spi4.MisoPin = GPIO_PIN_06;
    port_spi4.Misofunc = GPIO_FUNC_42_SPI4_MISO;
    /*初始化SPI*/
    bsp_spi_init(SPI_UNIT4,&spi_cfg);
    /*初始化DMA*/
    bsp_dma_init(SPI_DMA_UNIT,SPI_DMA_TX_CH,DMA_SRC_ADDR_INC,DMA_DEST_ADDR_FIX,DMA_DATAWIDTH_8BIT);
    bsp_dma_SetDesAddr(SPI_DMA_UNIT,SPI_DMA_TX_CH,(uint32_t)&(SPI_UNIT4->DR));
    bsp_dma_set_TrigSrc(SPI_DMA_UNIT,SPI_DMA_TX_CH,EVT_SPI4_SPTI);
    
    bsp_dma_init(SPI_DMA_UNIT,SPI_DMA_RX_CH,DMA_SRC_ADDR_FIX,DMA_DEST_ADDR_INC,DMA_DATAWIDTH_8BIT);
    bsp_dma_SetSrcAddr(SPI_DMA_UNIT,SPI_DMA_RX_CH,(uint32_t)&(SPI_UNIT4->DR));
    bsp_dma_set_TrigSrc(SPI_DMA_UNIT,SPI_DMA_RX_CH,EVT_SPI4_SPRI);
    /*初始化中断*/
    bsp_interrupt_callback_regist(INT_DMA1_TC6,DMA1_TC6_IRQn,bsp_spi4_dma_tc_callback);
    bsp_interrupt_callback_regist(INT_SPI4_SPEI, SPI4_IRQn, bsp_spi4_error_callback);
    return SPI_RET_OK;
}