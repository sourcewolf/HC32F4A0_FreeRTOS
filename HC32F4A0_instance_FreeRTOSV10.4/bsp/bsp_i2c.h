#ifndef BSP_I2C_H
#define BSP_I2C_H
#include "hc32_ddl.h"
#include "bsp_dma.h"
#include "bsp_interrupt.h"
#include "bsp_i2c_typedef.h"


#ifdef __cplusplus
extern "C" {
#endif
uint8_t bsp_i2c_init(M4_I2C_TypeDef* pstcI2Cx,uint32_t baudrate);
uint8_t bsp_i2c_deinit(M4_I2C_TypeDef* pstcI2Cx);
uint8_t I2C_Write_data(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr,uint8_t *addr, uint8_t addrsize, const uint8_t *data, uint16_t len);
uint8_t I2C_Read_data(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr,uint8_t *addr, uint8_t addrsize, uint8_t *data, uint16_t len);
uint8_t I2C_Write_Buffer(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr,const uint8_t *data, uint16_t len);
uint8_t I2C_Read_Buffer(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr, uint8_t *data, uint16_t len);
uint8_t I2C_DMA_Write_data(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr,uint8_t *addr, uint8_t addrsize, const uint8_t *data, uint16_t len);
uint8_t I2C_DMA_Read_data(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr,uint8_t* addr,uint8_t addrsize, uint8_t *data, uint16_t len);
uint8_t I2C_DMA_Write_Buffer(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr, const uint8_t *data, uint16_t len);
uint8_t I2C_DMA_Read_Buffer(M4_I2C_TypeDef* pstcI2Cx,uint8_t DeviceAddr, uint8_t *data, uint16_t len);
#ifdef __cplusplus
};
#endif

#endif

