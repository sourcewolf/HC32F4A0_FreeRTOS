#include "bsp_dac.h"

void bsp_dac_init(void)
{
    stc_gpio_init_t stcGpioInit;
    stc_dac_init_t stcInit;
    /* Set a default value. */
	GPIO_StructInit(&stcGpioInit);
    stcGpioInit.u16PinAttr = PIN_ATTR_ANALOG;
    GPIO_Unlock();
	GPIO_Init(GPIO_PORT_A, GPIO_PIN_04, &stcGpioInit);
	GPIO_Lock();
    DAC_StructInit(&stcInit);
    
	
    /* 2. Enable ADC peripheral clock. */
    PWC_Unlock(PWC_UNLOCK_CODE_1);
	PWC_Fcg3PeriphClockCmd(PWC_FCG3_DAC1, Enable);//��ADC1ʱ��
    /* Enable MAU peripheral clock. */
//    PWC_Fcg0PeriphClockCmd(PWC_FCG0_CORDIC, Enable);
	PWC_Lock(PWC_UNLOCK_CODE_1);
    
    DAC_Init(M4_DAC1,DAC_CH_1,&stcInit);
    DAC_DataRegAlignConfig(M4_DAC1,DAC_DATA_ALIGN_R);
    DAC_AMPCmd(M4_DAC1,DAC_CH_1,Enable);
    DAC_Start(M4_DAC1,DAC_CH_1);
}

void bsp_dac_set_value(uint16_t data)
{
    DAC_SetChannel1Data(M4_DAC1,data&0x0FFF);
}



