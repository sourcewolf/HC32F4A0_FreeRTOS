#ifndef BSP_UART_H
#define BSP_UART_H

#include "hc32_ddl.h"
#include "bsp_interrupt.h"
#include "bsp_dma.h"
#include "bsp_timer0.h"
#define RXLEN 10u
//#define UART_RXDMA_EN
#define UART_TIMEROUT 10
/******************************UART1**********************************/
#define UART1_RX_PORT   GPIO_PORT_H
#define UART1_RX_PIN    GPIO_PIN_14
#define UART1_RX_FUNC   GPIO_FUNC_33_USART1_RX

#define UART1_TX_PORT   GPIO_PORT_H
#define UART1_TX_PIN    GPIO_PIN_15
#define UART1_TX_FUNC   GPIO_FUNC_32_USART1_TX

#define UART1_RX_DMA    M4_DMA1
#define UART1_RX_CH     DMA_CH0
#define UART1_DMA_TRIG  EVT_USART1_RI
/***********************************************************************/
/******************************UART4**********************************/
#define UART4_RX_PORT   GPIO_PORT_A
#define UART4_RX_PIN    GPIO_PIN_14
#define UART4_RX_FUNC   GPIO_FUNC_39_USART4_RX

#define UART4_TX_PORT   GPIO_PORT_A
#define UART4_TX_PIN    GPIO_PIN_13
#define UART4_TX_FUNC   GPIO_FUNC_32_USART4_TX

#define UART4_RX_DMA    M4_DMA1
#define UART4_RX_CH     DMA_CH1
#define UART4_DMA_TRIG  EVT_USART4_RI
/***********************************************************************/


typedef struct
{
    stc_usart_uart_init_t stcUartConf;//UART参数
    uint8_t RTO_enable;//使能超时功能
    uint8_t rxPort;
    uint16_t rxPin;
    uint8_t rxfunc;//RX功能
    uint8_t txPort;
    uint16_t txPin;
    uint8_t txfunc;//TX功能
    uint32_t Baudrate;//波特率
    void* uart_err_callback;
    void* uart_timeout_callback;
    void* uart_rx_callback;
#ifdef UART_RXDMA_EN    
    M4_DMA_TypeDef* DmaUnit;
    uint8_t DmaCh;
    en_event_src_t DmaTrigsrc;
    void* rx_dma_callback;
#endif
}bsp_uart_cfg_t;

#ifdef __cplusplus
extern "C" {
#endif
uint8_t bsp_uart_init(M4_USART_TypeDef *USARTx,bsp_uart_cfg_t *uart_cfg);
extern uint8_t bsp_usart_transfer_wait(M4_USART_TypeDef *USARTx,uint8_t *data, uint32_t len);
void bsp_uart1_init(void);
void bsp_uart4_init(void);
#ifdef __cplusplus
};
#endif

#endif