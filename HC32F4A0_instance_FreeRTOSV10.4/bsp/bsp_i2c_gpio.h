#ifndef BSP_I2C_GPIO_H
#define BSP_I2C_GPIO_H
#include <hc32_ddl.h>

#define SCL_DIR bM4_GPIO->PCRD3_b.POUTE
#define SDA_DIR bM4_GPIO->PCRF10_b.POUTE
#define SCL_Pin_out bM4_GPIO->PODRD_b.POUT03
#define SDA_Pin_out bM4_GPIO->PODRF_b.POUT10
#define SDA_Pin_in bM4_GPIO->PIDRF_b.PIN10

//�ⲿ����
void Set_SCL_DIR(void);
void Set_SDA_DIR(unsigned char dir);
void Set_SCL_pin(bool value);
void Set_SDA_pin(bool value);
unsigned char Get_SDA_pin(void);
void Gpio_I2C_Init(void);
void Gpio_I2C_Start(void);
unsigned char  I2C_Write(unsigned char);
unsigned char I2C_Read(unsigned char);
unsigned char  I2C_GetAck(void);
void I2C_PutAck(unsigned char);
void Gpio_I2C_Stop(void);

unsigned char I2C_Send_Command(uint8_t DeviceAddr, uint8_t address, uint8_t *data, uint8_t len);
unsigned char I2C_Read_Command(uint8_t DeviceAddr, uint8_t address, uint8_t *rxbuf, uint8_t len);
#endif
