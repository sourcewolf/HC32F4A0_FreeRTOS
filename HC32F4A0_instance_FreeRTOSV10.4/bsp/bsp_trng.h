#ifndef BSP_TRNG_H
#define BSP_TRNG_H
#include "hc32_ddl.h"

#ifdef __cplusplus
extern "C" {
#endif
void bsp_trng_init(void);
void bsp_trng_gen(uint32_t *u32TrngArr,uint32_t count);
#ifdef __cplusplus
};
#endif
#endif
