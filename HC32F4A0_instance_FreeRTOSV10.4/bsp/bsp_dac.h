#ifndef BSP_DAC_H
#define BSP_DAC_H
#include "hc32_ddl.h"







#ifdef __cplusplus
extern "C" {
#endif

void bsp_dac_init(void);
void bsp_dac_set_value(uint16_t data);



#ifdef __cplusplus
};
#endif


#endif