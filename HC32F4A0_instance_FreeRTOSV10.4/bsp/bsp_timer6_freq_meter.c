#include "bsp_timer6_freq_meter.h"
/****************************************
Ƶ������ܽ�PF12

***************************************/
static uint8_t overflow_cnt = 0;
uint32_t Freq;
//void EXINT4_Callback(void)
//{
//	;
//}
void timer6_1ms_callback(void)
{
	;
}
void timer6_overflow_callback(void)
{
	overflow_cnt++;
}
void timer6_capture_callback(void)
{
	Freq = (CNT_TMR_UNIT->GCMAR)*10;//���10HZ���źŷ��������27MHZ�������Բ�á�
}
/**
 *******************************************************************************
 ** \brief PB04 input init function
 **
 ** \param  None
 **
 ** \retval None
 **
 ******************************************************************************/
void ExtPort_Init(void)
{
    stc_exint_init_t stcExintInit;
//    stc_irq_signin_config_t stcIrqRegiConf;
    stc_gpio_init_t stcGpioInit;
	GPIO_Unlock();
	GPIO_SetDebugPort(0x1C,Disable);
    /* configuration structure initialization */
   /* GPIO config */
    GPIO_StructInit(&stcGpioInit);
	stcGpioInit.u16PinDir = PIN_DIR_IN;
	stcGpioInit.u16ExInt = PIN_EXINT_ON;
    stcGpioInit.u16PullUp = PIN_PU_OFF;
	stcGpioInit.u16PinIType = PIN_ITYPE_CMOS;
	stcGpioInit.u16PinDrv = PIN_DRV_HIGH;
    GPIO_Init(PORT_FREQ_IN, PIN_FREQ_IN, &stcGpioInit);
    /**************************************************************************/
    /* External Int Ch.4                                                      */
    /**************************************************************************/
	EXINT_StructInit(&stcExintInit);
	stcExintInit.u32ExIntCh = EXITCH_FREQ;
    stcExintInit.u32ExIntLvl = EXINT_TRIGGER_FALLING;
	stcExintInit.u32ExIntFBE = EXINT_FILTER_B_OFF;
	stcExintInit.u32ExIntFAE = EXINT_FILTER_A_OFF;
	stcExintInit.u32ExIntFAClk = EXINT_FACLK_HCLK_DIV1;
	stcExintInit.u32ExIntFBTime = EXINT_FBTIM_500NS;
    EXINT_Init(&stcExintInit);
	GPIO_Lock();
//	stcIrqRegiConf.enIRQn = Int005_IRQn;                    /* Register INT_TMR61_GOVF Int to Vect.No.002 */
//    stcIrqRegiConf.enIntSrc = INT_FREQ;               /* Select Event interrupt of tmr61 */
//    stcIrqRegiConf.pfnCallback = &EXINT4_Callback;   /* Callback function */
//    INTC_IrqSignIn(&stcIrqRegiConf);                        /* Registration IRQ */

//    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);            /* Clear Pending */
//    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);/* Set priority */
////    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);                  /* Enable NVIC */

}
void bsp_timer6_timer_1ms_init(void)//��ʱTIMER��ʼ��
{
	uint32_t                       u32Period;
    stc_tmr6_basecnt_cfg_t         stcTIM6BaseCntCfg;
	stc_irq_signin_config_t        stcIrqRegiConf;
	TMR6_BaseCntStructInit(&stcTIM6BaseCntCfg);
	stcTIM6BaseCntCfg.u32CntMode = TMR6_MODE_SAWTOOTH;
	stcTIM6BaseCntCfg.u32CntDir = TMR6_CNT_INCREASE;
    stcTIM6BaseCntCfg.u32CntClkDiv = TMR6_CLK_PCLK0_DIV1;
    stcTIM6BaseCntCfg.u32CntStpAftOvf = TMR6_STOP_AFTER_OVF;//TMR6_CNT_CONTINUOUS;
	PWC_Unlock(PWC_UNLOCK_CODE_1);
	PWC_Fcg2PeriphClockCmd(T_TMR_EN, Enable);//��TIMER6ʱ��
	PWC_Lock(PWC_UNLOCK_CODE_1);
	
	TMR6_Init(T_TMR_UNIT, &stcTIM6BaseCntCfg);
	u32Period = 0x16E3600U;//100ms
    TMR6_SetPeriodReg(T_TMR_UNIT, TMR6_PERIOD_REG_A, u32Period);	
	TMR6_CountCmd(T_TMR_UNIT, Enable);	
	
	TMR6_IntCmd(T_TMR_UNIT, TMR6_IRQ_EN_OVERFLOW, Enable);
	TMR6_HwStartCondCmd(T_TMR_UNIT,TMR6_HW_CNT_INTER_EVENT0,Enable);
	TMR6_HwStartFuncCmd(T_TMR_UNIT, Enable);
	
//	stcIrqRegiConf.enIRQn = TIMER62_OVF_IRQn;                    /* Register INT_TMR61_GOVF Int to Vect.No.002 */
//    stcIrqRegiConf.enIntSrc = T_TMR_INT;               /* Select Event interrupt of tmr61 */
//    stcIrqRegiConf.pfnCallback = &timer6_1ms_callback;   /* Callback function */
//    INTC_IrqSignIn(&stcIrqRegiConf);                        /* Registration IRQ */

//    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);            /* Clear Pending */
//    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);/* Set priority */
//    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);                  /* Enable NVIC */
}
void bsp_timer6_counter_init(void)
{
	uint32_t                       u32Period;
    stc_tmr6_basecnt_cfg_t         stcTIM6BaseCntCfg;
    stc_gpio_init_t                stcGpioCfg;
    stc_irq_signin_config_t        stcIrqRegiConf;
    stc_tmr6_port_input_cfg_t      stcTIM6PortInCfg;
	stc_tmr6_port_output_cfg_t     stcTIM6PortOutCfg;
	
	TMR6_BaseCntStructInit(&stcTIM6BaseCntCfg);
    TMR6_PortInputStructInit(&stcTIM6PortInCfg);
	GPIO_StructInit(&stcGpioCfg);
	TMR6_PortOutputStructInit(&stcTIM6PortOutCfg);
	
    /* Unlock PWC register: FCG0 */
    PWC_FCG0_Unlock();
    /* Unlock PWC, CLK, PVD registers, @ref PWC_REG_Write_Unlock_Code for details */
    PWC_Unlock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);
	
	
	PWC_Fcg2PeriphClockCmd(CNT_TMR_EN, Enable);
	PWC_Fcg0PeriphClockCmd(PWC_FCG0_AOS, Enable);
	
	TMR6_DeInit(CNT_TMR_UNIT);
	stcTIM6BaseCntCfg.u32CntMode = TMR6_MODE_SAWTOOTH;
    stcTIM6BaseCntCfg.u32CntDir = TMR6_CNT_INCREASE;
    stcTIM6BaseCntCfg.u32CntClkDiv = TMR6_CLK_PCLK0_DIV1;
    stcTIM6BaseCntCfg.u32CntStpAftOvf = TMR6_CNT_CONTINUOUS;
    TMR6_Init(CNT_TMR_UNIT, &stcTIM6BaseCntCfg);
	CNT_TMR_UNIT->PCNAR |= 0x80000000;
	/* Period register set */
    u32Period = 0xFFFFFFFFU;
    TMR6_SetPeriodReg(CNT_TMR_UNIT, TMR6_PERIOD_REG_A, u32Period);
	
	TMR6_IntCmd(CNT_TMR_UNIT, TMR6_IRQ_EN_OVERFLOW, Enable);
	TMR6_IntCmd(CNT_TMR_UNIT, TMR6_IRQ_EN_CNT_MATCH_A, Enable);
	TMR6_SetTriggerSrc(TMR6_HW_TRIG_0,EVT_TMR6_2_GOVF);
	TMR6_HwIncreaseCondCmd(CNT_TMR_UNIT,TMR6_HW_CNT_TRIGEC_FAILLING,Enable);//�ڲ��¼�0������
//	TMR6_HwTrigEventCfg(TMR6_HW_TRIG_1, EVT_TMR6_2_GOVF);//TIMER62����¼���ΪӲ������Դ��
	TMR6_HwCaptureChACondCmd(CNT_TMR_UNIT, TMR6_HW_CNT_INTER_EVENT0,Enable);//�ڲ��¼�1������A
	TMR6_HwClrCondCmd(CNT_TMR_UNIT, TMR6_HW_CNT_INTER_EVENT0,Enable);//�ڲ��¼�1������
	TMR6_HwClrFuncCmd(CNT_TMR_UNIT, Enable);
	
    /* Lock PWC register: FCG0 */
    PWC_FCG0_Lock();
    /* Lock PWC, CLK, PVD registers, @ref PWC_REG_Write_Unlock_Code for details */
    PWC_Lock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);
	
	
	stcIrqRegiConf.enIRQn = TIMER63_CAP_IRQn;                    /* Register INT_TMR61_GOVF Int to Vect.No.002 */
    stcIrqRegiConf.enIntSrc = CNT_CAPT_INT;               /* Select Event interrupt of tmr61 */
    stcIrqRegiConf.pfnCallback = &timer6_capture_callback;   /* Callback function */
    INTC_IrqSignIn(&stcIrqRegiConf);                        /* Registration IRQ */

    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);            /* Clear Pending */
    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);/* Set priority */
    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);  

	stcIrqRegiConf.enIRQn = TIMER63_OVF_IRQn;                    /* Register INT_TMR61_GOVF Int to Vect.No.002 */
    stcIrqRegiConf.enIntSrc = CNT_OVERFLOW_INT;               /* Select Event interrupt of tmr61 */
    stcIrqRegiConf.pfnCallback = &timer6_overflow_callback;   /* Callback function */
    INTC_IrqSignIn(&stcIrqRegiConf);                        /* Registration IRQ */

    NVIC_ClearPendingIRQ(stcIrqRegiConf.enIRQn);            /* Clear Pending */
    NVIC_SetPriority(stcIrqRegiConf.enIRQn, DDL_IRQ_PRIORITY_15);/* Set priority */
    NVIC_EnableIRQ(stcIrqRegiConf.enIRQn);
	TMR6_CountCmd(CNT_TMR_UNIT, Enable);	
}





