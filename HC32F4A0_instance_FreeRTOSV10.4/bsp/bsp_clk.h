#ifndef BSP_CLK_H
#define BSP_CLK_H
#include "hc32_ddl.h"


#define ENABLE	1u
#define DISABLE	0u

#define XTAL_OSCMODE     CLK_XTALMODE_OSC
#define XTAL_CLOCKMODE   CLK_XTALMODE_EXCLK

#define XTALDRV_H   CLK_XTALDRV_HIGH//驱动能力高
#define XTALDRV_M   CLK_XTALDRV_MID//驱动能力中
#define XTALDRV_L   CLK_XTALDRV_LOW//驱动能力低
#define XTALDRV_XL  CLK_XTALDRV_ULOW//超低驱动能力

#define XTALSTB_35CLK       1u
#define XTALSTB_67CLK       2u
#define XTALSTB_131CLK      3u
#define XTALSTB_259CLK      4u
#define XTALSTB_547CLK      5u
#define XTALSTB_1059CLK     6u
#define XTALSTB_2147CLK     7u
#define XTALSTB_4291CLK     8u
#define XTALSTB_8163CLK     9u

#define XTAL32DRV_H   CLK_XTAL32DRV_HIGH//驱动能力高
#define XTAL32DRV_M   CLK_XTAL32DRV_MID//驱动能力中
#define XTAL32DRV_L   CLK_XTAL32DRV_LOW//驱动能力低
#define XTAL32DRV_XL  CLK_XTAL32DRV_ULOW//超低驱动能力

#define XTAL32NF_ALWAYS_ON  CLK_XTAL32NF_FULL
#define XTAL32NF_Sleep_STOP CLK_XTAL32NF_PART
#define XTAL32NF_OFF        CLK_XTAL32NF_NONE

#define PLL_CLK_SOURCE_HRC  CLK_PLLSRC_HRC
#define PLL_CLK_SOURCE_XTAL CLK_PLLSRC_XTAL

#define PLL_DIV2    1u
#define PLL_DIV3    2u
#define PLL_DIV4    3u
#define PLL_DIV5    4u
#define PLL_DIV6    5u
#define PLL_DIV7    6u
#define PLL_DIV8    7u
#define PLL_DIV9    8u
#define PLL_DIV10   9u
#define PLL_DIV11   10u
#define PLL_DIV12   11u
#define PLL_DIV13   12u
#define PLL_DIV14   13u
#define PLL_DIV15   14u
#define PLL_DIV16   15u
//----------以下分频只适用于输入分频---------------//
#define PLL_DIV17   16u
#define PLL_DIV18   17u
#define PLL_DIV19   18u
#define PLL_DIV20   19u
#define PLL_DIV21   20u
#define PLL_DIV22   21u
#define PLL_DIV23   22u
#define PLL_DIV24   23u

#define SYSCLK_HRC      CLK_SYSCLKSOURCE_HRC
#define SYSCLK_MRC      CLK_SYSCLKSOURCE_MRC
#define SYSCLK_LRC      CLK_SYSCLKSOURCE_LRC
#define SYSCLK_XTAL     CLK_SYSCLKSOURCE_XTAL
#define SYSCLK_XTAL32   CLK_SYSCLKSOURCE_XTAL32
#define SYSCLK_MPLL     CLK_SYSCLKSOURCE_PLLH

#define SYSCLK_DIV1     0u
#define SYSCLK_DIV2     1u
#define SYSCLK_DIV4     2u
#define SYSCLK_DIV8     3u
#define SYSCLK_DIV16    4u
#define SYSCLK_DIV32    5u
#define SYSCLK_DIV64    6u

#define USBCLK_SYS_DIV2 CLK_USB_CLK_MCLK_DIV2
#define USBCLK_SYS_DIV3 CLK_USB_CLK_MCLK_DIV3
#define USBCLK_SYS_DIV4 CLK_USB_CLK_MCLK_DIV4
#define USBCLK_SYS_DIV5 CLK_USB_CLK_MCLK_DIV5
#define USBCLK_SYS_DIV6 CLK_USB_CLK_MCLK_DIV6
#define USBCLK_SYS_DIV7 CLK_USB_CLK_MCLK_DIV7
#define USBCLK_SYS_DIV8 CLK_USB_CLK_MCLK_DIV8
#define USBCLK_PLLHQ   	CLK_USB_CLK_PLLHQ
#define USBCLK_PLLHR   	CLK_USB_CLK_PLLHR
#define USBCLK_PLLAP  	CLK_USB_CLK_PLLAP
#define USBCLK_PLLAQ   	CLK_USB_CLK_PLLAQ
#define USBCLK_PLLAR   	CLK_USB_CLK_PLLAR

#define PERICLK_Default 0u
#define PERICLK_PLLHQ	8u
#define PERICLK_PLLHR   9u
#define PERICLK_PLLAP  10u
#define PERICLK_PLLAQ  11u
#define PERICLK_PLLAR  12u

//------------------Configure XTAL---------------------------//
#define XTAL_ENABLE         DISABLE
#define XTAL_SUPDRV_ENABLE  Enable
#define XTAL_MODE           XTAL_OSCMODE
#define XTAL_DRV            XTALDRV_H
#define XTAL_STB            XTALSTB_2147CLK
#define XTALSTDE_ENABLE     DISABLE
#define XTALSTDRIS_ENABLE   DISABLE
#define XTALSTDRE_ENABLE    DISABLE
#define XTALSTDIE_ENABLE    DISABLE

//------------------Configure XTAL32K-------------------------//
#define XTAL32_ENABLE           ENABLE
#define XTAL32_SUPDRV_ENABLE    Disable
#define XTAL32_DRV              XTAL32DRV_H
#define XTAL32_NF_MODE          XTAL32NF_ALWAYS_ON

//---------------------Configure HRC-------------------------//
#define HRC_ENABLE              ENABLE
//--------------------Configure MRC--------------------------//
#define MRC_ENABLE              ENABLE
//--------------------Configure LRC--------------------------//
#define LRC_ENABLE              ENABLE
//-------------------Configure MainCLK-----------------------//
#define SYSTEMCLKSOURCE         SYSCLK_MPLL
//HCLK for Core, SRAM, GPIO, MPU, DCU, QSPI, INTC
#define HCLK_DIV                SYSCLK_DIV1
//EXCKS MAX 84MHZ   for SDIO, CAN
#define EXCKS_DIV               SYSCLK_DIV4
//PCLK4 MAX 84MHZ for ADC Logic, TRNG
#define PCLK4S_DIV              SYSCLK_DIV2
//PCLK3 MAX 42MHZ for I2C, RTC, CMP, WDT, SWDT, 
#define PCLK3S_DIV              SYSCLK_DIV4 
//PCLK2 MAX 60MHZ for ADC convert
#define PCLK2S_DIV              SYSCLK_DIV4
//PCLK1 MAX 84MHz for SPI, USART, Timer0, TimerA, Timer4, Timer6 control Clock, EMB, CRC, HASH, AES, I2S
#define PCLK1S_DIV              SYSCLK_DIV2
//PCLK0 for Timer6 counter clock MAX 168MHZ
#define PCLK0S_DIV              SYSCLK_DIV1
//---------------------Configure PLL---------------------------//
//Configure PLL Clock Source
#define PLL_CLK_SOURCE  PLL_CLK_SOURCE_XTAL
//------------------Configure MPLL CLK-------------------------//
//Enable MPLL
#define MPLL_CLK_ENABLE  ENABLE
//MPLL Source Clock Divider HRC 16MHZ DIV2 8MHZ
#define MPLL_CLK_M_DIV   1//PLL_DIV2+1
//----------------倍频系数，最低倍频数20，最高80-----------------//
//MPLL 倍频系数 21x8MHZ = 168MHZ
#define MPLL_CLK_NUM        42u//75u//
//MPLL_P,Q,R分频
#define MPLL_CLK_P_DIV      PLL_DIV2+1//MPLL 预分频数
#define MPLL_CLK_Q_DIV      PLL_DIV6+1
#define MPLL_CLK_R_DIV      PLL_DIV15+1


//------------------Configure UPLL CLK------------------------//
#define UPLL_CLK_ENABLE  ENABLE
#define UPLL_CLK_SOURCE  PLL_CLK_SOURCE//必须与MPLL相同
#define UPLL_CLK_M_DIV   2//PLL_DIV2+1
//倍频系数，最低倍频数20，最高80/
#define UPLL_CLK_NUM      84u

#define UPLL_CLK_P_DIV      PLL_DIV7+1//UPLL 预分频数
#define UPLL_CLK_Q_DIV      PLL_DIV7+1
#define UPLL_CLK_R_DIV      PLL_DIV7+1

//-----------------Configure USB CLK------------------------//
#define USB_CLK_SOURCE      USBCLK_UPLL_P

//-----------------Configure AD & PERIPHERAL TRNG CLK---------------------//
#define PERI_CLK_SOURCE     PERICLK_MPLL_Q


#ifdef __cplusplus
extern "C" {
#endif

void Peripheral_WE(void);
void Peripheral_WP(void);
en_result_t XtalInit(void);
en_result_t Xtal32Init(void);
en_result_t PLLHInit(void);
void bsp_sys_clk_init(void);


#ifdef __cplusplus
};
#endif



#endif

