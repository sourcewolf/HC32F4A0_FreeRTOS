#include "bsp_swdt.h"
stc_swdt_init_t swdt_cfg;
void bsp_swdt_init(void)
{
    swdt_cfg.u32ClockDivision = SWDT_CLOCK_DIV1;
    swdt_cfg.u32CountCycle = SWDT_COUNTER_CYCLE_65536;
    swdt_cfg.u32RefreshRange = SWDT_RANGE_0TO100PCT;
    swdt_cfg.u32TrigType = SWDT_TRIG_EVENT_RESET;
    swdt_cfg.u32LPModeCountEn = SWDT_LPM_COUNT_CONTINUE;
    SWDT_Init(&swdt_cfg);
    SWDT_Feed();//软件启动看门狗必须是先喂狗
}