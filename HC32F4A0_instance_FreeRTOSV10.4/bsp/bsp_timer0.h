#ifndef BSP_TIM0_H
#define BSP_TIM0_H
#include "hc32_ddl.h"
#include "bsp_irqn.h"
#ifdef __cplusplus
extern "C" {
#endif


uint8_t bsp_timer0_init(M4_TMR0_TypeDef *Timer0x,uint8_t enCh,stc_tmr0_init_t* pstcBaseInit,uint8_t RTOMODE_en);
uint8_t bsp_timer0_start(M4_TMR0_TypeDef *Timer0x,uint8_t enCh);
uint8_t bsp_timer0_stop(M4_TMR0_TypeDef *Timer0x,uint8_t enCh);
void bsp_timer0_init_interrupt(void);
#ifdef __cplusplus
};
#endif

#endif
