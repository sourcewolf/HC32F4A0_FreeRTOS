#include "Task_start.h"

osThreadId_t Hd_Task_Start;
const osThreadAttr_t StartTask_attributes = {
  .name = "StartTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
void Task_START(void *param)
{  
    while(1)
    {
		bsp_led_toggle();
		ledcontrol(0xFF);//
		osDelay(500);
		ledcontrol(0x00);
		osDelay(500);
    }
}
void OS_Start(void)
{
	osKernelInitialize();
	SysTick_Config(240000);
	Hd_Task_Start = osThreadNew(Task_START,NULL, &StartTask_attributes );		    
    osKernelStart();
}
