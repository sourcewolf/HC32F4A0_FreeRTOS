#ifndef TASK_START_H
#define TASK_START_H
#include "cmsis_os2.h"
#include "hc32_ddl.h"
#include "bsp_include.h"
#include "AT24C02.h"
#include "TCA9539.h"
void OS_Start(void);

#endif
