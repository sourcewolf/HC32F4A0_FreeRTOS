#include "hc32_ddl.h"
#include "Task_start.h"
#include "bsp_clk.h"
#include "bsp_log.h"
#include "AT24C02.h"
#include "bsp_i2c.h"
#include "bsp_efm.h"
#include "bsp_rmu.h"
stc_clk_freq_t Clkdata;
uint8_t data[10];
uint32_t resetFlag;
#define TIME (0x1000000u)
#define REG_VERSION     *(uint32_t*)0X4004CC80
extern void xPortSysTickHandler (void);
void SysTick_IrqHandler(void)
{
    SysTick_IncTick();
    xPortSysTickHandler();
}
void bsp_printf_porting(void)
{
    GPIO_SetFunc(GPIO_PORT_H, GPIO_PIN_15, GPIO_FUNC_32_USART1_TX, PIN_SUBFUNC_DISABLE);
}
void system_hardware_init(void)
{
    SystemCoreClockUpdate();
	bsp_sys_clk_init(); 
    PWC_Unlock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);
    PWC_Fcg3PeriphClockCmd(PWC_FCG3_USART1,Enable);
    PWC_Lock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);
    GPIO_Unlock();
    DDL_PrintfInit(M4_USART1,115200,bsp_printf_porting);
    LOG("-----------Enter APP: %s %s---------------\r\n",__DATE__,__TIME__);
    LOG("-----------system initialed --------------\r\n");
    bsp_showCurrntEFMbank();    
    resetFlag = M4_RMU->RSTF0;
    bsp_show_resetcause(resetFlag);
    PWC_Unlock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);    
    RMU_ClrStatus(); 
    PWC_AdcBufCmd(Enable);
    PWC_AdcInternVolSel(PWC_AD_INTERN_REF);//内部ADC模拟输入选择内部基准1.1V    
    PWC_Lock(PWC_UNLOCK_CODE_0 | PWC_UNLOCK_CODE_1 | PWC_UNLOCK_CODE_2);   
    GPIO_Lock();
    bsp_gpio_init();
    bsp_i2c_init(I2C1_UNIT,100000);
	TCA9539_CFG(0,0);
}
int main(void)
{
    system_hardware_init();
	LOG("Chip Version :%d \r\n",REG_VERSION);
	CLK_GetClockFreq(&Clkdata);
    LOG("system colock freq %dMHz\r\n",Clkdata.hclkFreq/1000000);
    LOG("Pclk0 freq = %dMHz\r\n",Clkdata.pclk0Freq/1000000);
    LOG("Pclk1 freq = %dMHz\r\n",Clkdata.pclk1Freq/1000000);
    LOG("Pclk2 freq = %dMHz\r\n",Clkdata.pclk2Freq/1000000);
    LOG("Pclk3 freq = %dMHz\r\n",Clkdata.pclk3Freq/1000000);
    LOG("Pclk4 freq = %dMHz\r\n",Clkdata.pclk4Freq/1000000);
    LOG_INFO("turn on cache\r\n");
    cacheon();
    LOG_INFO("start freertos\r\n");
	OS_Start();
	while(1)
	{
		;
	}
}
